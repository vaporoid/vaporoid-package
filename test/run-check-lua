#! /usr/bin/env lua

local vaporoid = require("vaporoid")
local filename = arg[0] .. ".tmp"

local fn = {
  test_is_array = function ()
    do
      assert(    vaporoid.is_array({}))
      assert(    vaporoid.is_array({ 42 }))
      assert(    vaporoid.is_array({ 42; 43 }))
      assert(    vaporoid.is_array({ 42; 43; 44 }))
      assert(not vaporoid.is_array({ 42; nil; 44 }))
      assert(not vaporoid.is_array({ 42; foo = 43; 44 }))
      assert(not vaporoid.is_array({ 42; [43] = 44; 45 }))
      assert(not vaporoid.is_array(42))
      assert(not vaporoid.is_array("foo"))
    end
  end;

  test_array = function ()
    do
      local data = vaporoid.array.copy({ 42; 43 }, { 44; 45 }, { 46; 47 })
      assert(#data == 6)
    end

    do
      local data = vaporoid.array.push({ 42; 43 }, 44, 45, 46, 47)
      assert(#data == 6)
    end

    do
      local data = vaporoid.array.unshift({ 42; 43 }, 44, 45)
      assert(#data == 4)
      assert(data[1] == 44)
      assert(data[2] == 45)
      assert(data[3] == 42)
      assert(data[4] == 43)
    end

    do
      local data = { 42; 43; 44; 45; 46; 47 }
      assert(vaporoid.array.shift(data) == 42)
      assert(vaporoid.array.shift(data) == 43)
      assert(#data == 4)
    end

    do
      local sum = vaporoid.array.reduce({ 42; 43; 44; 45; 46 }, 0, function (ctx, v)
        return ctx + v
      end)
      assert(sum == 220)
    end

    do
      local count = 0
      local result = vaporoid.array.each({ 42; 43; 44; 45; 46 }, function (v)
        count = count + 1
      end)
      assert(result == true)
      assert(count == 5)
    end

    do
      local count = 0
      local result = vaporoid.array.each({ 42; 43; 44; 45; 46 }, function (v)
        count = count + 1
        return v < 44
      end)
      assert(result == false)
      assert(count == 3)
    end

    do
      local data = vaporoid.array.grep({ 42; 43; 44; 45; 46 }, function (v, i)
        return i % 2 == 1
      end)
      assert(#data == 3)
      assert(data[1] == 42)
      assert(data[2] == 44)
      assert(data[3] == 46)
    end
  end;

  test_table = function ()
    local data = { foo = 42; bar = 43; baz = 44 }

    do
      local sum = vaporoid.table.reduce(data, 0, function (ctx, v)
        return ctx + v
      end)
      assert(sum == 129)
    end

    do
      local count = 0
      local result = vaporoid.table.each(data, function ()
        count = count + 1
      end)
      assert(result == true)
      assert(count == 3)
    end

    do
      local count = 0
      local result = vaporoid.table.each(data, function ()
        count = count + 1
        return false
      end)
      assert(result == false)
      assert(count == 1)
    end

    do
      local keys = vaporoid.table.keys(data)
      table.sort(keys)
      assert(#keys == 3)
      assert(keys[1] == "bar")
      assert(keys[2] == "baz")
      assert(keys[3] == "foo")
    end

    do
      local values = vaporoid.table.values(data)
      table.sort(values)
      assert(#values == 3)
      assert(values[1] == 42)
      assert(values[2] == 43)
      assert(values[3] == 44)
    end
  end;

  test_pathname = function ()
    do
      local basename = vaporoid.pathname.basename

      -- SUSv4 basename(1)
      assert(basename("/usr/src/cmd/cat.c", ".c") == "cat")

      -- SUSv4 basename(3)
      assert(basename("/usr/lib")     == "lib")
      assert(basename("/usr/")        == "usr")
      assert(basename("/")            == "/")
      assert(basename("///")          == "/")
      assert(basename("//usr//lib//") == "lib")

      -- LDP_man-pages basename(3)
      assert(basename("/usr/lib") == "lib")
      assert(basename("/usr/")    == "usr")
      assert(basename("usr")      == "usr")
      assert(basename("/")        == "/")
      assert(basename(".")        == ".")
      assert(basename("..")       == "..")

      assert(basename("")   == "")   -- or "."
      assert(basename("//") == "//") -- or "/"

      assert(basename("foo.tar.gz", ".gz")     == "foo.tar")
      assert(basename("foo.tar.gz", ".tar.gz") == "foo")
      assert(basename("foo.tar.gz", ".tar")    == "foo.tar.gz")
      assert(basename("foo.tar.gz", ".bz2")    == "foo.tar.gz")

      assert(basename("bar", "")    == "bar")
      assert(basename("bar", "r")   == "ba")
      assert(basename("bar", "ar")  == "b")
      assert(basename("bar", "bar") == "bar")
    end

    do
      local dirname = vaporoid.pathname.dirname

      -- SUSv4 dirname(1)
      assert(dirname("/")        == "/")
      assert(dirname("//")       == "//") -- or "/"
      assert(dirname("/a/b/")    == "/a")
      assert(dirname("//a//b//") == "//a")
      assert(dirname("a")        == ".")
      assert(dirname("")         == ".")
      assert(dirname("/a")       == "/")
      assert(dirname("/a/b")     == "/a")
      assert(dirname("a/b")      == "a")

      -- SUSv4 dirname(3)
      assert(dirname("/usr/lib") == "/usr")
      assert(dirname("/usr/")    == "/")
      assert(dirname("usr")      == ".")
      assert(dirname("/")        == "/")
      assert(dirname(".")        == ".")
      assert(dirname("..")       == ".")
      assert(dirname("//foo")    == "//") -- or "/"
    end
  end;

  test_array_writer = function ()
    do
      local out = vaporoid.array_writer()
      assert(tostring(out) == "")
      assert(out:close() == true)
    end

    do
      local out = vaporoid.array_writer()
        :write("foo", 42)
        :write("bar")
        :write(43)
      assert(tostring(out) == "foo42bar43")
      assert(out:close() == true)
    end
  end;

  test_writer = function ()
    do
      local out = vaporoid.writer(vaporoid:array_writer())
        :write("foo", 42)
        :write("bar")
        :write(43)
        :write_format_line("%q", "baz")
      assert(tostring(out.out) == "foo42bar43\"baz\"\n")
      assert(out:close() == true)
    end
  end;

  test_dumper = function ()
    -- cyclic data
    do
      local result, message = pcall(function ()
        local data = {}
        data.foo = data
        data.bar = data
        vaporoid.dumper():dump(vaporoid.array_writer(), data)
      end)
      assert(result == false)
    end

    do
      local data1 = {
        { 42; 43; 44; 45 };
        { foo = 42; bar = 43; baz = 44; qux = 45 };
        {};
      }

      local dump1 = tostring(vaporoid.dumper():dump(vaporoid.array_writer():write("return "), data1):write("\n"))
      io.write(dump1)

      local data2 = assert(load(dump1))()
      assert(#data2 == 3)
      assert(#data2[1] == 4)
      assert(data2[1][1] == 42)
      assert(data2[1][2] == 43)
      assert(data2[1][3] == 44)
      assert(data2[1][4] == 45)
      assert(data2[2].foo == 42)
      assert(data2[2].bar == 43)
      assert(data2[2].baz == 44)
      assert(data2[2].qux == 45)
      assert(#data2[3] == 0)

      local dump2 = tostring(vaporoid.dumper():dump(vaporoid.array_writer():write("return "), data2):write("\n"))
      io.write(dump2)

      assert(dump1 == dump2)
    end

    do
      assert(tostring(vaporoid.dumper():dump(vaporoid.array_writer(), nil))   == [=[nil]=])
      assert(tostring(vaporoid.dumper():dump(vaporoid.array_writer(), 42))    == [=[42]=])
      assert(tostring(vaporoid.dumper():dump(vaporoid.array_writer(), "foo")) == [=["foo"]=])
      assert(tostring(vaporoid.dumper():dump(vaporoid.array_writer(), true))  == [=[true]=])
    end

    do
      local dump1 = [=[return {{},{42,43,44,45},{["bar"]=43,["baz"]=44,["foo"]=42,["qux"]=45}}]=]
      local data1 = assert(load(dump1))()

      local dumper = vaporoid.dumper()
      dumper.indent = ""
      dumper.newline = ""
      dumper.key_separator = "="
      dumper.field_separator = ","
      dumper.optional_separator = false
      local dump2 = tostring(dumper:dump(vaporoid.array_writer():write("return "), data1))

      io.write(dump1, "\n")
      io.write(dump2, "\n")
      assert(dump1 == dump2)
    end

    -- unstable order
    do
      local data1 = {
        { 42; 43; 44; 45 };
        { foo = 42; bar = 43; baz = 44; qux = 45 };
        {};
      }

      local dumper = vaporoid.dumper()
      dumper.stable = false
      local dump = tostring(dumper:dump(vaporoid.array_writer():write("return "), data1):write("\n"))
      io.write(dump)

      local data2 = assert(load(dump))()
      assert(#data2 == 3)
      assert(#data2[1] == 4)
      assert(data2[1][1] == 42)
      assert(data2[1][2] == 43)
      assert(data2[1][3] == 44)
      assert(data2[1][4] == 45)
      assert(data2[2].foo == 42)
      assert(data2[2].bar == 43)
      assert(data2[2].baz == 44)
      assert(data2[2].qux == 45)
      assert(#data2[3] == 0)
    end
  end;

  test_popen_read = function ()
    io.write(vaporoid.popen_read("uname", "*l"), "\n")
  end;

  test_html = function ()
    assert(vaporoid.html.escape("foo")        == "foo")
    assert(vaporoid.html.escape("foo", "bar") == "foobar")
    assert(vaporoid.html.escape("&<>\"")      == "&amp;&lt;&gt;&quot;")
    io.write(vaporoid.html.prologue("foo"), vaporoid.html.epilogue())
  end;

  test_shell = function ()
    assert(vaporoid.shell.quote("foo")           == [=['foo']=])
    assert(vaporoid.shell.quote("foo", "bar")    == [=['foobar']=])
    assert(vaporoid.shell.quote("'foo'bar'")     == [=[\''foo'\''bar'\']=])
    assert(vaporoid.shell.quote("''foo''bar''")  == [=[\'\''foo'\'\''bar'\'\']=])
  end;

  test_os = function ()
    io.write(vaporoid.os.system(), "\n")
    io.write(vaporoid.os.release(), "\n")
    io.write(vaporoid.os.version(), "\n")
    io.write(vaporoid.os.machine(), "\n")
    io.write(vaporoid.os.here(arg[0]), "\n")
    io.write(vaporoid.os.here("../tool/vaporoid-package"), "\n")

    local here = vaporoid.os.here(arg[0])
    assert(here:match("/test$"))
    assert(here == vaporoid.popen_read("pwd", "*l"))
  end;

  test_package = function ()
    io.write(vaporoid.package.root(), "\n")
    io.write(vaporoid.package.boot(), "\n")
    io.write(vaporoid.package.build(), "\n")
    io.write(vaporoid.package.prefix(), "\n")
    io.write(vaporoid.package.prefix_path(), "\n")
    io.write(vaporoid.package.site(), "\n")
  end;

  test_next = function ()
    io.write(vaporoid.os.getenv("PATH"), "\n")
    io.write(vaporoid.next.path("../tool/vaporoid-package"), "\n")
    assert(vaporoid.os.getenv("PATH") ~= vaporoid.next.path("../tool/vaporoid-package"))
  end;
}

for k, v in pairs(fn) do
  io.write(string.format("# %s\n", k))
  v()
end
