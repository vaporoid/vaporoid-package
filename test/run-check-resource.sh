save_PATH=$PATH

list="foo bar baz"
a=0
for i in $list
do
  a=`expr $a + 1`
done

VAPOROID_PACKAGE_PREFIX_PATH=/foo:/bar:/baz
. ../tool/vaporoid-package-resource

test "x$PATH" = "x/foo/bin:/bar/bin:/baz/bin:$save_PATH"

VAPOROID_PACKAGE_PREFIX_PATH=/qux
. ../tool/vaporoid-package-resource

test "x$PATH" = "x/qux/bin:$save_PATH"

b=0
for i in $list
do
  b=`expr $b + 1`
done

test "x$a" = "x$b"
