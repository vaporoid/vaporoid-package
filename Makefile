RM = rm -f

all:
	./tool/vaporoid-package

clean:
	$(RM) -r _boot _site
	(cd src && make clean && make clean-boost)
	(cd src/boot && make clean)
	(cd src/test && make clean)
