# KVM

KVM（さくらのクラウド）を使う際の覚書。

## OpenBSD 5.3

OpenBSD 5.3/i386を/bsdで起動すると頻繁にクラッシュした。マルチプロセッサ用の/bsd.mpで起動することで問題を回避した。

    sudo cp /bsd /bsd.sp
    sudo cp /bsd.mp /bsd

## Solaris 11.1

em0経由のTCP接続が不安定だった。ホスト側でTCPオフローディングが有効になっており、ドライバのバグが顕在化して問題が発生したようだ。この問題を回避するため、/kernel/drv/e1000g.confに下記を追加した。

    tx_hcksum_enable=0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
    lso_enable=0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;

ACPIが有効になっていると、ときどきシステムクロックがエポックになった。この問題を回避するため、ACPIを無効にした。

    sudo bootadm change-entry -i 0 kargs="-B acpi-user-options=0x2"

