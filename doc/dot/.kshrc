if test -f "$HOME/.resource"; then
  . "$HOME/.resource"
fi

PS1='$USER@`uname -n`:$PWD$ '
PS2='> '
