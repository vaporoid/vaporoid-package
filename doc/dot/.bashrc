if test -f "$HOME/.resource"; then
  . "$HOME/.resource"
fi

PS1='\u@\h:\w\$ '
PS2='> '
