if test -f "$HOME/.resource"; then
  . "$HOME/.resource"
fi

if autoload -U compinit; then
  compinit
fi

if autoload -U colors; then
  colors
  PS1="%{$fg[red]%}%n@%m:%~%#%{$reset_color%} "
  PS2="%{$fg[red]%}>%{$reset_color%} "
fi

setopt hist_ignore_dups
setopt share_history
HISTFILE=~/.zsh_history
HISTSIZE=65536
SAVEHIST=65536

setopt extended_glob

alias h='fc -l -i 1 | grep'
