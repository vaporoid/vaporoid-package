set ambiwidth=double
set encoding=utf-8
set fileencodings=iso-2022-jp-3
set fileencodings+=euc-jisx0213
set fileencodings+=cp932
set fileencodings+=utf-8
set fileformats=unix,mac,dos
set termencoding=utf-8

function! AutoCommand_BufReadPost_PreferredFileEncoding()
  if &fileencoding =~ '^iso-2022-jp'
    if search('[^\x01-\x7f]', 'ne') == 0
      let &fileencoding=&encoding
    endif
  endif
endfunction
autocmd BufReadPost * call AutoCommand_BufReadPost_PreferredFileEncoding()

set autoindent
set expandtab
set shiftwidth=2
set softtabstop=2

set formatoptions+=nmM
set textwidth=0
set nowrap

set hlsearch
set ignorecase
set incsearch
set showmatch
set smartcase
set wrapscan

set list
set listchars=tab:__,trail:_
set number

set nobackup

set modeline
set modelines=5

set laststatus=2
set statusline=%<%F\ %m%r%h%w%y%{'['.&fileencoding.']['.&fileformat.']'}
\%=[%l,%c]

set wildmode=list:longest

syntax enable
filetype plugin on
colorscheme darkblue

cnoremap <C-X> %:h
nnoremap j gj
nnoremap k gk

let g:netrw_sort_sequence='[\/]$'
