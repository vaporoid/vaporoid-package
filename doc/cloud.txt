alice

CentOSのディスク追加
  /dev/sdb
  sudo fdisk /dev/sdb
  sudo pvcreate /dev/sdb1
  sudo pvdisplay
  sudo vgextend vg_alice /dev/sdb1
  sudo vgdisplay
  sudo lvdisplay /dev/vg_alice/lv_root
  # sudo lvextend -l "+5118" /dev/vg_alice/lv_root
  sudo lvextend -l "+10238" /dev/vg_alice/lv_root
  sudo resize2fs /dev/vg_alice/lv_root

  http://centossrv.com/lvm-hdd-add.shtml
  http://centossrv.com/lvm-extend.shtml

solarisのディスク追加
  sudo format
  で確認 c7d0とc7d1
  sudo zpool create opt c7d1

OpenBSD
  bsd.mpを使う（KVM上でi386がフリーズする）

OpenBSD Ports
  export PKG_PATH=http://ftp.jaist.ac.jp/pub/OpenBSD/5.3/packages/i386/
  sudo pkg_add rsync
  sudo pkg_add curl
  sudo pkg_add zsh

OpenBSDのディスク追加
  sudo disklabel -E wd1
  sudo newfs wd1a
  sudo mkdir /opt
  sudo mount /dev/wd1a /opt
  /etc/fstab
    /dev/wd1a /opt ffs rw,softdep 1 1

openbsd50-x86_64.local
openbsd50-x86.local
centos6-x86.local
centos6-x64.local

openbsd
linux
sunos
darwin

alice
alice
kfZk6gNb

インスタンスが変わるとMACアドレスとIPアドレスが変わる。

さくらのクラウドの場合、NICはIntel PRO/1000ドライバを使う。
  OpenBSD em0
  Solaris net0 (e1000g)
  CentOS  eth0

OpenBSD
  /etc/hostname.em0
  /etc/myname
  /etc/mygate
  MACアドレスは自動的に変わる

  mount /dev/cd0a /mnt
  umount /mnt

Solaris
  MACアドレスは自動的に変わる（rootじゃなきゃ見れないけど）

  /etc/inet/hosts (/etc/hosts, /etc/inet/ipnodesの実体）
  /etc/netmasks（必要？）


  # IPアドレスの取得
  ipadm show-addr
  ipadm show-addr net0
  ipadm show-addr net0/v4

  # IPアドレスの設定
  ipadm create-ip net0
  ipadm create-addr -T static -a 133.242.23.68/24 net0/v4

  # IPアドレスの削除
  ipadm delete-addr net0/v4
  ipadm delete-ip net0

  # デフォルトゲートウェイ
  netstat -rn
  route get 8.8.8.8
  route -p add default 133.242.23.1
  route -p delete default 133.242.23.1

  svccfg -v -s svc:/network/dns/client setprop config/nameserver = net_address: '(133.242.0.3 133.242.0.4)'
  svccfg -v -s svc:/network/dns/client:default refresh
  svccfg -v -s svc:/network/dns/client:default validate
  svccfg -v -s svc:/system/name-service/switch setprop config/host = astring: '"files dns"'
  svccfg -v -s svc:/system/name-service/switch:default refresh
  svccfg -v -s svc:/system/name-service/switch:default validate
  svcadm -v enable svc:/network/dns/client
  svcadm -v refresh svc:/system/name-service/switch

  # ホスト名の変更
  svccfg -v -s svc:/system/identity:node setprop config/nodename = astring: 'alice.vaporoid.com'
  svcadm -v refresh svc:/system/identity:node
  svcadm -v restart svc:/system/identity:node
  /etc/inet/hostsを変更する

  # デフォルトゲートウェイ
  sudo route -p add default DEFAULT
  sudo route -p delete default DEFAULT

  http://docs.oracle.com/cd/E26924_01/html/E25934/gliyc.html
  http://docs.oracle.com/cd/E26924_01/html/E29114/ipadm-1m.html
  http://www.oracle.com/technetwork/articles/servers-storage-admin/s11-network-config-1632927.html




  9C:A3:BA:2B:13:D7
  133.242.53.250

  9C:A3:BA:23:A9:44
  133.242.20.13




svccfg -v -s svc:/system/name-service/switch listprop config
svccfg -v -s svc:/system/name-service/switch setprop config/host = astring: '"files dns"'
svccfg -v -s svc:/system/name-service/switch:default refresh
svccfg -v -s svc:/system/name-service/switch:default validate
svcadm -v refresh svc:/system/name-service/switch

svccfg -v -s svc:/network/dns/client listprop config
svccfg -v -s svc:/network/dns/client setprop config/nameserver = net_address: "($nameserver)"
svccfg -v -s svc:/network/dns/client:default refresh
svccfg -v -s svc:/network/dns/client:default validate
svcadm -v enable svc:/network/dns/client

cat /etc/nsswitch.conf
cat /etc/resolv.conf



