local dumper = require("vaporoid.dumper")
local html4  = require("vaporoid.html4")
local system = require("vaporoid.system")

local function package_root ()
  return system.getenv("VAPOROID_PACKAGE_ROOT")
end

return {
  dumper = dumper;
  html   = html4;
  html4  = html4;
  os     = system;
  system = system;

  array        = require("vaporoid.array");
  array_writer = require("vaporoid.array_writer");
  is_array     = require("vaporoid.is_array");
  pathname     = require("vaporoid.pathname");
  popen_read   = require("vaporoid.popen_read");
  shell        = require("vaporoid.shell");
  table        = require("vaporoid.table");
  twitter      = require("vaporoid.twitter");
  writer       = require("vaporoid.writer");

  package = {
    boot = function ()
      return package_root() .. "/_boot"
    end;

    root = package_root;

    build = function ()
      return system.getenv("VAPOROID_PACKAGE_BUILD")
    end;

    prefix = function ()
      return system.getenv("VAPOROID_PACKAGE_PREFIX")
    end;

    prefix_path = function ()
      return system.getenv("VAPOROID_PACKAGE_PREFIX_PATH")
    end;

    site = function ()
      return package_root() .. "/_site"
    end;
  };

  dump = function (...)
    return dumper():dump(...)
  end;

  next = {
    path = function (program)
      local here = system.here(program)
      local path = {}
      for v in system.getenv("PATH"):gmatch("[^:]+") do
        if v ~= here then
          table.insert(path, v)
        end
      end
      return table.concat(path, ":")
    end;
  };
}
