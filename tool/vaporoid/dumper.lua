local is_array     = require("vaporoid.is_array")
local array_writer = require("vaporoid.array_writer")

local prototype = {
  stable             = true;
  indent             = "  ";
  newline            = "\n";
  key_separator      = " = ";
  field_separator    = ";";
  optional_separator = true;
  max_depth          = 256;

  dump_indent = function (self, out, depth)
    for i = 1, depth do
      out = out:write(self.indent)
    end
    return out
  end;

  dump_array = function (self, out, data, depth)
    if #data == 0 then
      return out:write("{}")
    else
      out = out:write("{", self.newline)
      out = self:dump_indent(out, depth + 1)
      for i, v in ipairs(data) do
        if i > 1 then
          out = out:write(self.field_separator, self.newline)
          out = self:dump_indent(out, depth + 1)
        end
        out = self:dump(out, v, depth + 1)
      end
      if self.optional_separator then
        out = out:write(self.field_separator)
      end
      out = out:write(self.newline)
      out = self:dump_indent(out, depth)
      return out:write("}")
    end
  end;

  dump_stable = function (self, out, data, depth)
    local pair = {}
    for k, v in pairs(data) do
      table.insert(pair, { key = tostring(self:dump(array_writer(), k, depth + 1)); value = v })
    end
    table.sort(pair, function (a, b)
      return a.key < b.key
    end)

    out = out:write("{", self.newline)
    out = self:dump_indent(out, depth + 1)
    for i, v in ipairs(pair) do
      if i > 1 then
        out = out:write(self.field_separator, self.newline)
        out = self:dump_indent(out, depth + 1)
      end
      out = out:write("[", v.key, "]", self.key_separator)
      out = self:dump(out, v.value, depth + 1)
    end
    if self.optional_separator then
      out = out:write(self.field_separator)
    end
    out = out:write(self.newline)
    out = self:dump_indent(out, depth)
    return out:write("}")
  end;

  dump_unstable = function (self, out, data, depth)
    out = out:write("{", self.newline)
    out = self:dump_indent(out, depth + 1)
    local is_first = true
    for k, v in pairs(data) do
      if is_first then
        is_first = false
      else
        out = out:write(self.field_separator, self.newline)
        out = self:dump_indent(out, depth + 1)
      end

      out = out:write("[")
      out = self:dump(out, k, depth + 1)
      out = out:write("]", self.key_separator)
      out = self:dump(out, v, depth + 1)
    end
    if self.optional_separator then
      out = out:write(self.field_separator)
    end
    out = out:write(self.newline)
    out = self:dump_indent(out, depth)
    return out:write("}")
  end;

  dump = function (self, out, data, depth)
    if depth == nil then
      depth = 0
    end
    assert(depth <= self.max_depth)

    local data_type = type(data)
    if data_type == "number" then
      return out:write(string.format("%.17g", data))
    elseif data_type == "string" then
      return out:write(string.format("%q", data))
    elseif data_type == "boolean" then
      return out:write(data and "true" or "false")
    elseif data_type == "table" then
      if is_array(data) then
        return self:dump_array(out, data, depth)
      elseif self.stable then
        return self:dump_stable(out, data, depth)
      else
        return self:dump_unstable(out, data, depth)
      end
    else
      return out:write("nil")
    end
  end;
}

local class = {
  prototype = prototype;

  new = function (class)
    local self = {}
    for k, v in pairs(prototype) do
      self[k] = v
    end
    return self
  end;
}

local class_metatable = {
  __call = function (class, ...)
    return class:new(...)
  end;
}

return setmetatable(class, class_metatable)
