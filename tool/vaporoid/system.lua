local pathname   = require("vaporoid.pathname")
local popen_read = require("vaporoid.popen_read")
local shell      = require("vaporoid.shell")

local function system ()
  local system = popen_read("uname", "*l"):lower()
  assert(system == "darwin" or system == "linux" or system == "openbsd" or system == "sunos")
  return system
end

local function release ()
  return popen_read("uname -r", "*l"):lower()
end

return {
  system  = system;
  release = release;

  version = function ()
    local system = system()
    local name
    local version

    if system == "darwin" then
      name = popen_read("sw_vers -productName", "*l")
      version = popen_read("sw_vers -productVersion", "*l")
    elseif system == "linux" then
      do
        local h = assert(io.popen("lsb_release -is >/dev/null 2>&1"))
        if h then
          name = h:read("*l")
          if h:close() then
            version = popen_read("lsb_release -rs", "*l")
          else
            name = nil
          end
        end
      end

      if name == nil then
        local release = {
          "/etc/system-release";
          "/etc/redhat-release";
        }

        for i, v in ipairs(release) do
          local h = io.open(v)
          if h then
            local line = h:read("*l"):gsub("%s+", " "):lower()
            h:close()
            line = line:gsub(" release", "")
            line = line:gsub(" %(.-%)", "")
            name, version = line:match("(.*) (.*)")
            break
          end
        end
      end
    elseif system == "openbsd" then
      name = system
      version = release()
    elseif system == "sunos" then
      local h = assert(io.open("/etc/release"))
      local line = h:read("*l"):gsub("%s+", " "):gsub(" %a%w*$", ""):lower()
      h:close()
      name, version = line:match(" ?(.-) ([ 0-9./]+)")
      version = version:gsub("[ /]", ".")
    end

    assert(name ~= nil and version ~= nil)
    return string.format("%s-%s", name:gsub("%s+", "_"):lower(), version)
  end;

  machine = function ()
    local machine = popen_read("uname -m", "*l"):lower()
    if machine:find("^i%d86$") or machine == "i86pc" then
      return "x86"
    elseif machine == "amd64" then
      return "x86_64"
    end
    assert(machine == "x86" or machine == "x86_64")
    return machine
  end;

  getenv = function (key)
    local value = os.getenv(key)
    assert(type(value) == "string" and #value > 0)
    return value
  end;

  here = function (program)
    return popen_read(string.format("(cd %s && pwd)", shell.quote(pathname.dirname(program))), "*l")
  end;
}
