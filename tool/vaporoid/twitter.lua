local popen_read = require("vaporoid.popen_read")
local shell      = require("vaporoid.shell")

local function escape (...)
  return (table.concat({...}):gsub("[^0-9A-Za-z%-._~]", function (v)
    return string.format("%%%02X", v:byte())
  end))
end

local function unescape (...)
  return (table.concat({...}):gsub("+", " "):gsub("%%[0-9A-Fa-f][0-9A-Fa-f]", function (v)
    return string.char(tonumber(v:sub(-2), 16))
  end))
end

local DEBUG = false
local F = string.format
local Q = shell.quote
local E = escape
local U = unescape

local function nonce ()
  return popen_read("openssl rand -hex 16", "*l")
end

local function hmac_key (consumer_secret, token_secret)
  assert(consumer_secret ~= nil)
  if token_secret == nil then
    return F("%s&", consumer_secret)
  else
    return F("%s&%s", consumer_secret, token_secret)
  end
end

local function signature (method, uri, parameters, hmac_key)
  local p = {}
  for k, v in pairs(parameters) do
    table.insert(p, table.concat({ k, v }, "="))
  end
  table.sort(p, function (a, b)
    return a < b
  end)
  local signature_base_string = string.format("%s&%s&%s", E(method), E(uri), E(table.concat(p, "&")))
  if DEBUG then
    io.stderr:write(F("[DEBUG] signature_base_string: %s\n", signature_base_string))
  end
  local command = F("printf '%%s' %s | openssl sha1 -binary -hmac %s | openssl base64", Q(signature_base_string), Q(hmac_key))
  if DEBUG then
    io.stderr:write(F("[DEBUG] command: %s\n", command))
  end
  return popen_read(command, "*l")
end

local function authorization (parameters)
  local p = {}
  for k, v in pairs(parameters) do
    table.insert(p, table.concat({ k, F([["%s"]], E(v)) }, "="))
  end
  table.sort(p, function (a, b)
    return a < b
  end)
  local authorization = F("Authorization: OAuth %s", table.concat(p, ", "))
  if DEBUG then
    io.stderr:write(F("[DEBUG] authorization: %s\n", authorization))
  end
  return authorization
end

local function execute (method, uri, authorization, data)
  local command = F("curl --request %s %s --header %s --silent", Q(method), Q(uri), Q(authorization))
  if type(data) == "table" then
    for i, v in ipairs(data) do
      command = F("%s --data %s", command, v)
    end
  end
  if DEBUG then
    io.stderr:write(F("[DEBUG] command: %s\n", command))
  end
  return popen_read(command, "*a")
end

local function result (response)
  local r = {}
  for i in response:gmatch("[^&]+") do
    local k, v = i:match("([^=]*)=(.*)")
    assert(k ~= nil)
    assert(v ~= nil)
    r[U(k)] = U(v)
  end
  return r
end

local prototype = {
  -- consumer_key
  -- consumer_secret
  -- token
  -- token_secret

  escape = function (self, ...)
    return escape(...)
  end;

  unescape = function (self, ...)
    return unescape(...)
  end;

  request_token = function (self)
    assert(self.consumer_key    ~= nil)
    assert(self.consumer_secret ~= nil)

    local parameters = {
      oauth_consumer_key     = self.consumer_key;
      oauth_nonce            = nonce();
      oauth_signature_method = "HMAC-SHA1";
      oauth_timestamp        = os.time();
      oauth_version          = "1.0";
    }
    local method = "POST"
    local uri    = "https://api.twitter.com/oauth/request_token"

    parameters.oauth_signature = signature(method, uri, parameters, hmac_key(self.consumer_secret))
    local response = execute(method, uri, authorization(parameters))
    if DEBUG then
      io.stderr:write(F("[DEBUG] response: %s\n", response))
    end
    return result(response)
  end;

  access_token = function (self, verifier)
    assert(self.consumer_key    ~= nil)
    assert(self.consumer_secret ~= nil)
    assert(self.token           ~= nil)

    local parameters = {
      oauth_consumer_key     = self.consumer_key;
      oauth_nonce            = nonce();
      oauth_signature_method = "HMAC-SHA1";
      oauth_timestamp        = os.time();
      oauth_token            = self.token;
      oauth_version          = "1.0";
      oauth_verifier         = E(verifier);
    }
    local method = "POST"
    local uri    = "https://api.twitter.com/oauth/access_token"

    parameters.oauth_signature = signature(method, uri, parameters, hmac_key(self.consumer_secret))

    parameters.oauth_verifier = nil;
    local response = execute(method, uri, authorization(parameters), { "oauth_verifier=" .. E(verifier) })
    if DEBUG then
      io.stderr:write(F("[DEBUG] response: %s\n", response))
    end
    return result(response)
  end;

  update = function (self, status)
    assert(self.consumer_key    ~= nil)
    assert(self.consumer_secret ~= nil)
    assert(self.token           ~= nil)
    assert(self.token_secret    ~= nil)

    local parameters = {
      oauth_consumer_key     = self.consumer_key;
      oauth_nonce            = nonce();
      oauth_signature_method = "HMAC-SHA1";
      oauth_timestamp        = os.time();
      oauth_token            = self.token;
      oauth_version          = "1.0";
      status                 = E(status);
    }
    local method = "POST"
    local uri    = "https://api.twitter.com/1.1/statuses/update.json"

    parameters.oauth_signature = signature(method, uri, parameters, hmac_key(self.consumer_secret, self.token_secret))

    parameters.status = nil;
    local response = execute(method, uri, authorization(parameters), { "status=" .. E(status) })
    if DEBUG then
      io.stderr:write(F("[DEBUG] response: %s\n", response))
    end
  end;
}

local class = {
  escape    = escape;
  unescape  = unescape;
  prototype = prototype;

  new = function (class)
    local self = {}
    for k, v in pairs(prototype) do
      self[k] = v
    end
    return self
  end;
}

local class_metatable = {
  __call = function (class, ...)
    return class:new(...)
  end;
}

return setmetatable(class, class_metatable)
