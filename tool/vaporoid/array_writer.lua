local prototype = {
  open = function (self, ...)
    self.out = ...
    return self
  end;

  close = function (self)
    self.out = nil
    return true
  end;

  write = function (self, ...)
    for i, v in ipairs({...}) do
      table.insert(self.out, v)
    end
    return self
  end;

  string = function (self)
    return table.concat(self.out)
  end;
}

local self_metatable = {
  __tostring = function (self)
    return self:string()
  end;
}

local class = {
  prototype = prototype;

  new = function (class)
    local self = {}
    for k, v in pairs(prototype) do
      self[k] = v
    end
    return setmetatable(self:open({}), self_metatable)
  end;
}

local class_metatable = {
  __call = function (class, ...)
    return class:new(...)
  end;
}

return setmetatable(class, class_metatable)
