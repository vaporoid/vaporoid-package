return function (source)
  if type(source) == "table" then
    local max, count = 0, 0
    for k, v in pairs(source) do
      if type(k) == "number" and k > 0 and math.floor(k) == k then
        max = math.max(max, k)
        count = count + 1
      else
        return false
      end
    end
    return max == count
  else
    return false
  end
end
