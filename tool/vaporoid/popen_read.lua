return function (command, format)
  local h = assert(io.popen(command))
  local data = h:read(format)
  assert(h:close())
  return data
end
