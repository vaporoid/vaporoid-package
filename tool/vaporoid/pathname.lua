return {
  basename = function (filename, suffix)
    -- step 1
    if #filename == 0 then
      return ""
    end

    -- step 2
    if filename == "//" then
      return "//"
    end

    -- step 3
    if filename:match("^/+$") then
      return "/"
    end

    -- step 4
    filename = filename:gsub("/+$", "")

    -- step 5
    filename = filename:gsub("^.*/", "")

    -- step 6
    if suffix ~= nil and suffix ~= filename then
      if filename:sub(-#suffix) == suffix then
        filename = filename:sub(1, #filename - #suffix)
      end
    end

    return filename
  end;

  dirname = function (filename)
    -- step 1
    if filename == "//" then
      return "//"
    end

    -- step 2
    if filename:match("^/+$") then
      return "/"
    end

    -- step 3
    filename = filename:gsub("/+$", "")

    -- step 4
    if filename:find("/", 1, true) == nil then
      return "."
    end

    -- step 5
    filename = filename:gsub("[^/]+$", "")

    -- step 6
    if filename == "//" then
      return "//"
    end

    -- step 7
    filename = filename:gsub("/+$", "")

    -- step 8
    if #filename == 0 then
      return "/"
    end

    return filename
  end;
}
