local function escape (...)
  local data = table.concat({...})
  data = data:gsub([=[&]=], "&amp;")
  data = data:gsub([=["]=], "&quot;")
  data = data:gsub([=[<]=], "&lt;")
  data = data:gsub([=[>]=], "&gt;")
  return data
end

local function prologue (name)
  return string.format([=[
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>%s</title>
    <style type="text/css">
      table {
        border-collapse: collapse;
      }
      th, td {
        border-color: #333;
        border-style: solid;
        border-width: 1px;
        padding: 0.25em;
      }
    </style>
  </head>
  <body>
]=], escape(name))
end

return {
  escape = escape;

  prologue_without_h1 = prologue;

  prologue = function (name)
    return prologue(name) .. string.format([=[
    <h1>%s</h1>
]=], escape(name))
  end;

  epilogue = function ()
    return [=[
  </body>
</html>
]=]
  end;
}
