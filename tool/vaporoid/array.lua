local function copy (target, source)
  for i, v in ipairs(source) do
    table.insert(target, v)
  end
  return target
end

return {
  copy = function (target, ...)
    for i, v in ipairs({...}) do
      target = copy(target, v)
    end
    return target
  end;

  push = function (target, ...)
    return copy(target, {...})
  end;

  unshift = function (target, ...)
    for i, v in ipairs({...}) do
      table.insert(target, i, v)
    end
    return target
  end;

  shift = function (target)
    return table.remove(target, 1)
  end;

  reduce = function (source, init, fn)
    for i, v in ipairs(source) do
      init = fn(init, v, i)
    end
    return init
  end;

  each = function (source, fn)
    for i, v in ipairs(source) do
      if fn(v, i) == false then
        return false
      end
    end
    return true
  end;

  grep = function (source, fn)
    local target = {}
    for i, v in ipairs(source) do
      if fn(v, i) then
        table.insert(target, v)
      end
    end
    return target
  end;
}
