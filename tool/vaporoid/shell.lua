return {
  quote = function (...)
    local data = table.concat({...})
    data = data:gsub([=[']=], [=['\'']=])
    data = [=[']=] .. data .. [=[']=]
    data = data:gsub([=[''\']=], [=[\']=])
    data = data:gsub([=[\''']=], [=[\']=])
    return data
  end;
}
