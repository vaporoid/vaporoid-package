local dumper = require("vaporoid.dumper")

local prototype = {
  open = function (self, ...)
    self.out = ...
    self.once = {}
    return self
  end;

  close = function (self)
    return self.out:close()
  end;

  write = function (self, ...)
    self.out:write(...)
    return self
  end;

  write_format_line = function (self, ...)
    return self:write(string.format(...), "\n")
  end;

  write_format_line_once = function (self, ...)
    local line = string.format(...)
    if self.once[line] == nil then
      self.once[line] = true
      return self:write(line, "\n")
    else
      return self
    end
  end;

  dump = function (...)
    return dumper():dump(...)
  end;
}

local class = {
  prototype = prototype;

  new = function (class, ...)
    local self = {}
    for k, v in pairs(prototype) do
      self[k] = v
    end
    return self:open(...)
  end;
}

local metatable = {
  __call = function (class, ...)
    return class:new(...)
  end;
}

return setmetatable(class, metatable)
