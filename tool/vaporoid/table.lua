return {
  reduce = function (source, init, fn)
    for k, v in pairs(source) do
      init = fn(init, v, k)
    end
    return init
  end;

  each = function (source, fn)
    for k, v in pairs(source) do
      if fn(v, k) == false then
        return false
      end
    end
    return true
  end;

  keys = function (source)
    local target = {}
    for k, v in pairs(source) do
      table.insert(target, k)
    end
    return target
  end;

  values = function (source)
    local target = {}
    for k, v in pairs(source) do
      table.insert(target, v)
    end
    return target
  end;
}
