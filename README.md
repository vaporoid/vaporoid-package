# vaporoid-package

## 概念

### ブートストラップ

ブートストラップのため、私はUNIXの部分集合を仮定する。UNIXそれ自体の輪郭はPOSIXと呼ばれたものに似ている。仮定されるUNIXの部分集合は、もっぱらGNU autoconfのマニュアルに典拠する。GNU autoconf 2.69のマニュアルは、

* Portable Shell Programming
* Portable Make Programming
* Portable C and C++ Programming

の三節を可搬性の解説に割いている（かつて、Makeに独立した節は与えられていなかった）。これらの三節が示す三要素、Shell, Make, C/C++は重要である。Makeは、しかし、ShellとC/C++ほどに重要ではない。ShellとC/C++はシステムの界面でありつづけてきた。Makeは初手からそのように産まれなかった。ファイルの更新時刻を調べ、依存関係を解決するツールでしかなかった。

すべてを一からビルドするためにMakeは必須ではない。修正とビルドが繰りかえされなければ、Makeの強力さは発揮されえない。すべてを一からビルドするのであれば、コンパイルされ、リンクされるべきファイルのリストがあれば充分だ。

しかし、Makeは使われてきた。可搬なビルドシステムのいくつかは可搬なMakefileを生成しさえする。もうひとつ、ここには誤謬がある。可搬なビルドシステムのほとんどは、開発中のソースコードと配布されたソースコードを同じ方法でビルドしようとする。それとこれは別個の目的を持つ別個の行為であるにも関わらず。

しかし、Makeは使われてきた。だが、ブートストラップに際してMake（なかんずく拡張機能）を使いこなすべきではないとだけは言えるだろう。GNU makeの拡張機能を使いたければGNU makeをビルドすれば良いのではあるけれど。

残念なことに、可搬なShellはとても貧弱である。たとえば可搬にメッセージダイジェストを計算する方法はないし、そもそも文字列操作を満足に行いえない。単純な解決策がある。可搬なシェルスクリプトと可搬なC/C++で貧弱さを補えばよい。

だから、私はLuaを使うことにした。共有リンクライブラリを使わない限り、このうえもなくLuaは可搬だ。Luaのビルドで可搬でないのはGNU installに依存する部分くらいしかない。Luaに足りない機能があれば、可搬なC++で書けば必要なものが揃う。

1. 可搬なシェルスクリプト
2. 可搬なCで書かれたLua
3. 可搬なC++で書かれたユーティリティ

## ビルド

    cd tool
    ./vaporoid-package -c vaporoid-package-batch /opt/vaporoid

## 2013年6月に確認した環境

| システム                         | x86_64 | x86 |
| ---------------------------------| :----: | :-: |
| Amazon Linux AMI release 2013.03 |   X    |     |
| CentOS  6.4                      |   X    |  X  |
| MacOS X 10.6.8                   |   X    |     |
| MacOS X 10.8.3                   |   X    |     |
| OpenBSD 5.3                      |   X    |  X  |
| Oracle Solaris 11.1              |        |  X  |

### CentOS 6

最小構成では下記のRPMを追加する必要があるかもしれない。

* gcc
* gcc-c++
* make

許容するシステムライブラリは下記のRPMに含まれている。

* glibc
* libacl
* libattr
* libgcc
* libstdc++

### MacOS X

Mac OS X 10.7以降、アップルが提供する開発ツールは（LLVM-GCCでない）本物のGCCを含まない。[osx-gcc-installer][]等を用いて、本物のGCCをインストールするべきである。GCCあるいはOTPのビルドは、本物のGCCを必要とする。

[osx-gcc-installer]: https://github.com/kennethreitz/osx-gcc-installer/

### システム構成 (OpenBSD 5.3)

* base53.tgz
* etc53.tgz
* comp53.tgz
* man53.tgz

### Oracle Solaris 11

テキストインストールでは下記のパッケージを追加する必要があるかもしれない。

* pkg:/compatibility/ucb
* pkg:/developer/gcc-45
* pkg:/system/header

#### 参考

互換性と標準準拠に関連するパッケージには、以下のようなパッケージがある。

* pkg:/compatibility/ucb
* pkg:/system/xopen/xcu4
* pkg:/system/xopen/xcu6
* pkg:/system/header
* pkg:/system/library

pkgコマンドの例を示す。

    pkg contents pkg:/compatibility/ucb
    pkg info pkg:/compatibility/ucb
    pkg search /usr/ucb/expr

