#ifndef VAPOROID_DIRECTORY_HANDLE_HPP
#define VAPOROID_DIRECTORY_HANDLE_HPP

#include <dirent.h>
#include <unistd.h>
#include <vector>
#include <boost/noncopyable.hpp>
#include "system_error.hpp"

namespace vaporoid {
  namespace directory_handle_detail {
    class directory_handle : boost::noncopyable {
    public:
      explicit directory_handle(const char* dirname)
        : handle_() {
        long name_max = pathconf(dirname, _PC_NAME_MAX);
        if (name_max == -1) {
          throw system_error("Could not pathconf");
        }
        entry_.resize(offsetof(struct dirent, d_name) + name_max + 1);

        handle_ = opendir(dirname);
        if (!handle_) {
          throw system_error("Could not opendir");
        }
      }

      ~directory_handle() {
        if (handle_) {
          closedir(handle_);
        }
      }

      const char* read() {
        struct dirent* result = 0;
        if (readdir_r(handle_, reinterpret_cast<struct dirent*>(&entry_[0]), &result) == -1) {
          throw system_error("Could not readdir_r");
        }
        return result ? result->d_name : 0;
      }

    private:
      DIR* handle_;
      std::vector<char> entry_;
    };
  }

  using directory_handle_detail::directory_handle;
}

#endif
