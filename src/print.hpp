#ifndef VAPOROID_PRINT_HPP
#define VAPOROID_PRINT_HPP

#include <string.h>
#include <iomanip>
#include <iostream>
#include <string>
#include <boost/io/ios_state.hpp>
#include "stdint.hpp"

namespace vaporoid {
  namespace print_detail {
    template <typename T>
    inline void print_hex(std::ostream& out, T i, T end) {
      boost::io::ios_all_saver save(out);
      out << std::hex << std::setfill('0');
      for (; i != end; ++i) {
        out << std::setw(2) << static_cast<uint16_t>(static_cast<uint8_t>(*i));
      }
    }

    template <typename T>
    inline void print_hex(std::ostream& out, T value) {
      boost::io::ios_all_saver save(out);
      out << std::hex << std::setfill('0') << std::setw(sizeof(T) * 2) << static_cast<uintmax_t>(value);
    }

    template <typename T>
    inline void print_lua_quote(std::ostream& out, T i, T end) {
      out << "\"";
      for (; i != end; ++i) {
        switch (*i) {
          case '\0': out << "\\0";  break;
          case '\a': out << "\\a";  break;
          case '\b': out << "\\b";  break;
          case '\f': out << "\\f";  break;
          case '\n': out << "\\n";  break;
          case '\r': out << "\\r";  break;
          case '\t': out << "\\t";  break;
          case '\v': out << "\\v";  break;
          case '\\': out << "\\\\"; break;
          case '"':  out << "\\\""; break;
          case '\'': out << "\\'";  break;
          default:   out << *i;
        }
      }
      out << "\"";
    }

    inline void print_lua_quote(std::ostream& out, const char* value) {
      print_lua_quote(out, value, value + strlen(value));
    }

    inline void print_lua_quote(std::ostream& out, const std::string& value) {
      print_lua_quote(out, value.begin(), value.end());
    }
  }

  using print_detail::print_hex;
  using print_detail::print_lua_quote;
}

#endif
