#ifndef VAPOROID_FORK_HPP
#define VAPOROID_FORK_HPP

#include <unistd.h>
#include "system_error.hpp"

namespace vaporoid {
  inline pid_t fork() {
    pid_t pid = ::fork();
    if (pid == -1) {
      throw system_error("Could not fork");
    }
    return pid;
  }
}

#endif
