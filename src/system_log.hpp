#ifndef VAPOROID_SYSTEM_LOG_HPP
#define VAPOROID_SYSTEM_LOG_HPP

#include <syslog.h>
#include <sstream>
#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/repetition/enum_binary_params.hpp>
#include <boost/preprocessor/repetition/enum_params.hpp>
#include <boost/preprocessor/repetition/repeat.hpp>
#include <boost/preprocessor/repetition/repeat_from_to.hpp>

namespace vaporoid {
  namespace system_log_detail {
    inline void system_log(int priority, const char* message) { // noexcept
      syslog(priority, "%s\n", message);
    }

#define VAPOROID_SYSTEM_LOG_PP_print(PP_z, PP_i, PP_data) \
    out << BOOST_PP_CAT(a, PP_i);

#define VAPOROID_SYSTEM_LOG_PP_macro(PP_z, PP_i, PP_data) \
    template <BOOST_PP_ENUM_PARAMS(PP_i, typename T)> \
    inline void system_log(int priority, BOOST_PP_ENUM_BINARY_PARAMS(PP_i, const T, & a)) { \
      std::ostringstream out; \
      BOOST_PP_REPEAT(PP_i, VAPOROID_SYSTEM_LOG_PP_print, _) \
      system_log(priority, out.str().c_str()); \
    }

    BOOST_PP_REPEAT_FROM_TO(1, 17, VAPOROID_SYSTEM_LOG_PP_macro, _)

#undef VAPOROID_SYSTEM_LOG_PP_print
#undef VAPOROID_SYSTEM_LOG_PP_macro
  }

  using system_log_detail::system_log;
}

#endif
