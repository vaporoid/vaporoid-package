#include <stddef.h>
#include <exception>
#include <iostream>
#include <string>
#include <vector>
#include "file_handle.hpp"
#include "hash.hpp"
#include "print.hpp"
#include "stdint.hpp"

namespace {
  using vaporoid::uint8_t;

  inline void print_help(std::ostream& out, const char* program) {
    out << "SYNOPSIS\n"
        << "  " << program << " algorithm [file]\n"
        << "  " << program << " -h\n"
        << "\n"
        << "OPTIONS\n"
        << "  -h         Print a description of the command line options.\n"
        << "\n"
        << "OPERANDS\n"
        << "  algorithm  Print the FIPS 180-3 SHS message digest.\n"
        << "             (sha1|sha224|sha256|sha384|sha512)\n"
        << "  file       The file to digest.\n"
        << "             (Default: the standard input)\n"
        << "\n";
  }

  inline bool run(const std::string& algorithm, std::ostream& out, vaporoid::file_handle& h) {
    vaporoid::hash hash(algorithm);

    std::vector<char> buffer(4096);
    while (true) {
      size_t read_size = h.read(&buffer[0], buffer.size());
      if (read_size > 0) {
        hash.update(&buffer[0], read_size);
      }
      if (read_size < buffer.size()) {
        break;
      }
    }

    std::vector<uint8_t> md(hash.message_digest_size());
    hash.finalize(&md[0]);
    vaporoid::print_hex(out, md.begin(), md.end()); out << "\n";

    return true;
  }
}

int main(int argc, char* argv[]) {
  try {
    if (argc < 2) {
      print_help(std::cout, argv[0]);
      return 1;
    }

    vaporoid::file_handle h(stdin);
    if (argc > 2 && *argv[2] != '\0') {
      vaporoid::file_handle(argv[2], "rb").swap(h);
    }

    if (!run(argv[1], std::cout, h)) {
      print_help(std::cout, argv[0]);
      return 1;
    }
    return 0;
  } catch (const std::exception& e) {
    std::cerr << e.what() << "\n";
  } catch (...) {
    std::cerr << "Unknown exception\n";
  }
  return 1;
}
