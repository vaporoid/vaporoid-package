#include <fcntl.h>
#include <paths.h>
#include <signal.h>
#include <stddef.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <syslog.h>
#include <time.h>
#include <unistd.h>
#include <exception>
#include <iostream>
#include <set>
#include <sstream>
#include <string>
#include <vector>
#include <boost/array.hpp>
#include <boost/foreach.hpp>
#include <boost/noncopyable.hpp>
#include <boost/none.hpp>
#include <boost/optional.hpp>
#include <boost/utility/in_place_factory.hpp>
#include "dup2.hpp"
#include "execute.hpp"
#include "file_control.hpp"
#include "fork.hpp"
#include "pipe.hpp"
#include "scoped_closer.hpp"
#include "scoped_signal_action.hpp"
#include "scoped_signal_mask.hpp"
#include "system_error.hpp"
#include "system_log.hpp"

extern "C" {
  volatile sig_atomic_t caught_sigchld = 0;
  volatile sig_atomic_t caught_sighup  = 0;
  volatile sig_atomic_t caught_sigint  = 0;
  volatile sig_atomic_t caught_sigquit = 0;
  volatile sig_atomic_t caught_sigterm = 0;
  volatile sig_atomic_t caught_sigusr1 = 0;
  volatile sig_atomic_t caught_sigusr2 = 0;

  int self_pipe_reader = -1;
  int self_pipe_writer = -1;

  void handle_sigchld(int) { caught_sigchld = 1; write(self_pipe_writer, "C", 1); }
  void handle_sighup (int) { caught_sighup  = 1; write(self_pipe_writer, "H", 1); }
  void handle_sigint (int) { caught_sigint  = 1; write(self_pipe_writer, "I", 1); }
  void handle_sigquit(int) { caught_sigquit = 1; write(self_pipe_writer, "Q", 1); }
  void handle_sigterm(int) { caught_sigterm = 1; write(self_pipe_writer, "T", 1); }
  void handle_sigusr1(int) { caught_sigusr1 = 1; write(self_pipe_writer, "1", 1); }
  void handle_sigusr2(int) { caught_sigusr2 = 1; write(self_pipe_writer, "2", 1); }
}

namespace {
  static const int LOG_ERROR   = LOG_ERR;
  static const int LOG_VERBOSE = LOG_INFO;

  struct options {
    bool   foreground;
    bool   verbose;
    time_t timeout;
  };

  inline void print_help(std::ostream& out, const char* program) {
    out << "SYNOPSIS\n"
        << "  " << program << " [-d <delimiter>] [-f] [-t <timeout>] [-v] main... <delimiter> pipe... [<delimiter> exit...]\n"
        << "  " << program << " -h\n"
        << "\n"
        << "OPTIONS\n"
        << "  -d <delimiter>  Set the delimiter to the string <delimiter>.\n"
        << "                  (Default: \",\")\n"
        << "  -f              Run as the foreground process.\n"
        << "                  (Default: run as the background process)\n"
        << "  -h              Print a description of the command line options.\n"
        << "  -t <timeout>    Set the timeout in seconds.\n"
        << "                  (Default: no timeout)\n"
        << "  -v              Turn on the verbose mode.\n"
        << "\n"
        << "OPERANDS\n"
        << "  main...         The main command and its arguments to be invoked.\n"
        << "  pipe...         The pipe command and its arguments to be invoked.\n"
        << "  exit...         The exit command and its arguments to be invoked.\n"
        << "\n";
  }

  inline time_t time() {
    time_t t = ::time(0);
    if (t == -1) {
      throw vaporoid::system_error("Could not time");
    }
    return t;
  }

  class scoped_signal_actions : boost::noncopyable {
  public:
    explicit scoped_signal_actions()
      : sigchld_(SIGCHLD, handle_sigchld),
        sighup_ (SIGHUP,  handle_sighup),
        sigint_ (SIGINT,  handle_sigint),
        sigquit_(SIGQUIT, handle_sigquit),
        sigterm_(SIGTERM, handle_sigterm),
        sigusr1_(SIGUSR1, handle_sigusr1),
        sigusr2_(SIGUSR2, handle_sigusr2),
        sigpipe_(SIGPIPE, SIG_IGN) {}

  private:
    vaporoid::scoped_signal_action sigchld_;
    vaporoid::scoped_signal_action sighup_;
    vaporoid::scoped_signal_action sigint_;
    vaporoid::scoped_signal_action sigquit_;
    vaporoid::scoped_signal_action sigterm_;
    vaporoid::scoped_signal_action sigusr1_;
    vaporoid::scoped_signal_action sigusr2_;
    vaporoid::scoped_signal_action sigpipe_;
  };

  inline void kill_children(const std::set<pid_t>& children_pid, int sig) {
    BOOST_FOREACH(pid_t i, children_pid) {
      if (kill(i, sig) == -1) {
        throw vaporoid::system_error("Could not kill");
      }
    }
  }

  inline int run(const std::vector<std::string>& main_args, const std::vector<std::string>& pipe_args, const std::vector<std::string>& exit_args, const options& opts) {
    sigset_t signal_set;
    sigemptyset(&signal_set);
    sigaddset(&signal_set, SIGCHLD);
    sigaddset(&signal_set, SIGHUP);
    sigaddset(&signal_set, SIGINT);
    sigaddset(&signal_set, SIGQUIT);
    sigaddset(&signal_set, SIGTERM);
    sigaddset(&signal_set, SIGUSR1);
    sigaddset(&signal_set, SIGUSR2);
    boost::optional<vaporoid::scoped_signal_mask> signal_block(boost::in_place(SIG_BLOCK, &signal_set));

    vaporoid::pipe(self_pipe_reader, self_pipe_writer);
    boost::optional<vaporoid::scoped_closer> close_self_pipe_reader(boost::in_place(self_pipe_reader));
    boost::optional<vaporoid::scoped_closer> close_self_pipe_writer(boost::in_place(self_pipe_writer));
    if (opts.verbose) {
      vaporoid::system_log(LOG_VERBOSE, "self_pipe_reader: ", self_pipe_reader);
      vaporoid::system_log(LOG_VERBOSE, "self_pipe_writer: ", self_pipe_writer);
    }
    vaporoid::file_control::set_nonblock(self_pipe_reader);
    vaporoid::file_control::set_nonblock(self_pipe_writer);

    boost::optional<scoped_signal_actions> signal_actions(boost::in_place());

    int   result   = 1;
    pid_t pipe_pid = 0;
    pid_t main_pid = 0;
    pid_t exit_pid = 0;
    std::set<pid_t> children_pid;

    {
      int stdio_pipe_reader = -1;
      int stdio_pipe_writer = -1;
      vaporoid::pipe(stdio_pipe_reader, stdio_pipe_writer);
      if (opts.verbose) {
        vaporoid::system_log(LOG_VERBOSE, "stdio_pipe_reader: ", stdio_pipe_reader);
        vaporoid::system_log(LOG_VERBOSE, "stdio_pipe_writer: ", stdio_pipe_writer);
      }
      boost::optional<vaporoid::scoped_closer> close_stdio_pipe_reader(boost::in_place(stdio_pipe_reader));
      boost::optional<vaporoid::scoped_closer> close_stdio_pipe_writer(boost::in_place(stdio_pipe_writer));

      {
        pipe_pid = vaporoid::fork();
        if (pipe_pid == 0) {
          try {
            vaporoid::dup2(stdio_pipe_reader, STDIN_FILENO);
            close_stdio_pipe_reader = boost::none;
            close_stdio_pipe_writer = boost::none;
            signal_actions          = boost::none;
            close_self_pipe_reader  = boost::none;
            close_self_pipe_writer  = boost::none;
            signal_block            = boost::none;
            if (opts.verbose) {
              for (size_t i = 0; i < pipe_args.size(); ++i) {
                vaporoid::system_log(LOG_VERBOSE, "pipe_args[", i, "]: ", pipe_args[i]);
              }
            }
            vaporoid::execute(pipe_args);
          } catch (const std::exception& e) {
            vaporoid::system_log(LOG_ERROR, e.what());
          } catch (...) {
            vaporoid::system_log(LOG_ERROR, "Unknown exception");
          }
          _exit(1);
        } else {
          if (opts.verbose) {
            vaporoid::system_log(LOG_VERBOSE, "pipe_pid: ", pipe_pid);
          }
          children_pid.insert(pipe_pid);
        }
      }

      {
        main_pid = vaporoid::fork();
        if (main_pid == 0) {
          try {
            vaporoid::dup2(stdio_pipe_writer, STDOUT_FILENO);
            vaporoid::dup2(stdio_pipe_writer, STDERR_FILENO);
            close_stdio_pipe_reader = boost::none;
            close_stdio_pipe_writer = boost::none;
            signal_actions          = boost::none;
            close_self_pipe_reader  = boost::none;
            close_self_pipe_writer  = boost::none;
            signal_block            = boost::none;
            if (opts.verbose) {
              for (size_t i = 0; i < main_args.size(); ++i) {
                vaporoid::system_log(LOG_VERBOSE, "main_args[", i, "]: ", main_args[i]);
              }
            }
            vaporoid::execute(main_args);
          } catch (const std::exception& e) {
            vaporoid::system_log(LOG_ERROR, e.what());
          } catch (...) {
            vaporoid::system_log(LOG_ERROR, "Unknown exception");
          }
          _exit(1);
        } else {
          if (opts.verbose) {
            vaporoid::system_log(LOG_VERBOSE, "main_pid: ", main_pid);
          }
          children_pid.insert(main_pid);
        }
      }
    }

    time_t deadline = 0;
    if (opts.timeout > 0) {
      deadline = opts.timeout + time();
      if (setenv("VAPOROID_PACKAGE_SPAWN_TIMEOUT", "FALSE", 1) == -1) {
        throw vaporoid::system_error("Could not setenv");
      }
    } else {
      if (unsetenv("VAPOROID_PACKAGE_SPAWN_TIMEOUT") == -1) {
        throw vaporoid::system_error("Could not unsetenv");
      }
    }

    int state = 0;
    while (state < 4) {
      fd_set read_fd_set;
      FD_ZERO(&read_fd_set);
      FD_SET(self_pipe_reader, &read_fd_set);

      struct timeval select_timeout = {};
      select_timeout.tv_sec = 1;
      int select_result = -1;
      {
        boost::optional<vaporoid::scoped_signal_mask> signal_unblock(boost::in_place(SIG_UNBLOCK, &signal_set));
        select_result = select(self_pipe_reader + 1, &read_fd_set, 0, 0, &select_timeout);
      }
      if (opts.verbose) {
        vaporoid::system_log(LOG_VERBOSE, "select_result: ", select_result);
      }

      if (select_result == -1) {
        if (errno == EINTR) {
          if (opts.verbose) {
            vaporoid::system_log(LOG_VERBOSE, "select: EINTR");
          }
          continue;
        } else {
          throw vaporoid::system_error("Could not select");
        }
      } else if (select_result == 0) {
        if (opts.verbose) {
          vaporoid::system_log(LOG_VERBOSE, "select: timeout");
        }
      }

      if (FD_ISSET(self_pipe_reader, &read_fd_set)) {
        while (true) {
          char c = 0;
          int read_result = read(self_pipe_reader, &c, 1);
          if (read_result == -1) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
              if (opts.verbose) {
                vaporoid::system_log(LOG_VERBOSE, "read: EAGAIN");
              }
              break;
            } else {
              throw vaporoid::system_error("Could not read");
            }
          } else if (read_result == 0) {
            if (opts.verbose) {
              vaporoid::system_log(LOG_VERBOSE, "read: EOF");
            }
            break;
          } else {
            if (opts.verbose) {
              vaporoid::system_log(LOG_VERBOSE, "read: ", c);
            }
          }
        }
      }

      if (caught_sigchld) {
        caught_sigchld = 0;
        while (true) {
          int status = 0;
          pid_t pid = waitpid(-1, &status, WNOHANG);
          if (pid == -1) {
            if (errno == ECHILD) {
              if (opts.verbose) {
                vaporoid::system_log(LOG_VERBOSE, "waitpid: ECHILD");
              }
              break;
            } else {
              throw vaporoid::system_error("Could not waitpid");
            }
          } else if (pid == 0) {
            if (opts.verbose) {
              vaporoid::system_log(LOG_VERBOSE, "waitpid: EOF");
            }
            break;
          }

          if (opts.verbose) {
            if (WIFEXITED(status)) {
              vaporoid::system_log(LOG_VERBOSE, "waitpid: ", pid, " exit ", WEXITSTATUS(status));
            } else if (WIFSIGNALED(status)) {
              vaporoid::system_log(LOG_VERBOSE, "waitpid: ", pid, " signal ", WTERMSIG(status));
            } else {
              vaporoid::system_log(LOG_VERBOSE, "waitpid: ", pid, " status ", status);
            }
          }

          if (main_pid == pid) {
            if (WIFEXITED(status)) {
              result = WEXITSTATUS(status);
            }
          }

          children_pid.erase(pid);
          ++state;
        }
      }

      if (opts.verbose) {
        vaporoid::system_log(LOG_VERBOSE, "children_pid.size(): ", children_pid.size());
      }

      if (caught_sighup)  { caught_sighup  = 0; kill_children(children_pid, SIGHUP); }
      if (caught_sigint)  { caught_sigint  = 0; kill_children(children_pid, SIGINT); }
      if (caught_sigquit) { caught_sigquit = 0; kill_children(children_pid, SIGQUIT); }
      if (caught_sigterm) { caught_sigterm = 0; kill_children(children_pid, SIGTERM); }
      if (caught_sigusr1) { caught_sigusr1 = 0; kill_children(children_pid, SIGUSR1); }
      if (caught_sigusr1) { caught_sigusr2 = 0; kill_children(children_pid, SIGUSR2); }

      if (state < 2) {
        if (deadline > 0 && deadline < time()) {
          if (opts.verbose) {
            vaporoid::system_log(LOG_VERBOSE, "deadline");
          }
          deadline = 0;
          if (setenv("VAPOROID_PACKAGE_SPAWN_TIMEOUT", "TRUE", 1) == -1) {
            throw vaporoid::system_error("Could not setenv");
          }
          kill_children(children_pid, SIGTERM);
        }
      } else if (state == 2) {
        ++state;
        if (exit_args.empty()) {
          ++state;
        } else {
          exit_pid = vaporoid::fork();
          if (exit_pid == 0) {
            try {
              signal_actions         = boost::none;
              close_self_pipe_reader = boost::none;
              close_self_pipe_writer = boost::none;
              signal_block           = boost::none;

              if (opts.verbose) {
                for (size_t i = 0; i < exit_args.size(); ++i) {
                  vaporoid::system_log(LOG_VERBOSE, "exit_args[", i, "]: ", exit_args[i]);
                }
              }
              vaporoid::execute(exit_args);
            } catch (const std::exception& e) {
              vaporoid::system_log(LOG_ERROR, e.what());
            } catch (...) {
              vaporoid::system_log(LOG_ERROR, "Unknown exception");
            }
            _exit(1);
          } else {
            if (opts.verbose) {
              vaporoid::system_log(LOG_VERBOSE, "exit_pid: ", exit_pid);
            }
            children_pid.insert(exit_pid);
          }
        }
      }
    }

    return result;
  }

  template <typename T>
  inline void convert(const std::string& source, T& target) {
    std::istringstream in(source);
    in >> target;
  }
}

extern int   optind;
extern char* optarg;

int main(int argc, char* argv[]) {
  options opts = {};
  std::vector<std::vector<std::string> > args(1);

  try {
    std::string delimiter = ",";

    while (true) {
      int c = getopt(argc, argv, "d:fht:v");
      if (c == -1) {
        break;
      }
      switch (c) {
        case 'd':
          delimiter = optarg;
          break;
        case 'f':
          opts.foreground = true;
          break;
        case 'h':
          print_help(std::cout, argv[0]);
          return 0;
        case 't':
          convert(optarg, opts.timeout);
          break;
        case 'v':
          opts.verbose = true;
          break;
        case '?':
          print_help(std::cout, argv[0]);
          return 1;
      }
    }

    for (int i = optind; i < argc; ++i) {
      if (argv[i] == delimiter) {
        args.push_back(std::vector<std::string>());
      } else {
        args.back().push_back(argv[i]);
      }
    }

    if (   args.size() < 2
        || args.size() > 3
        || args[0].size() == 0
        || args[1].size() == 0
        || (args.size() == 3 && args[2].size() == 0)) {
      print_help(std::cout, argv[0]);
      return 1;
    }

    if (args.size() == 2) {
      args.push_back(std::vector<std::string>());
    }
  } catch (const std::exception& e) {
    std::cerr << e.what() << "\n";
    return 1;
  } catch (...) {
    std::cerr << "Unknown exception\n";
    return 1;
  }

  int result = 1;
  try {
    if (!opts.foreground) {
      pid_t pid = vaporoid::fork();
      if (pid == 0) {
        if (setsid() == -1) {
          throw vaporoid::system_error("Could not setsid");
        }

        if (chdir("/") == -1) {
          throw vaporoid::system_error("Could not chdir");
        }

        int fd = open(_PATH_DEVNULL, O_RDWR);
        if (fd == -1) {
          throw vaporoid::system_error("Could not open");
        }
        vaporoid::scoped_closer closer(fd);

        vaporoid::dup2(fd, STDIN_FILENO);
        vaporoid::dup2(fd, STDOUT_FILENO);
        vaporoid::dup2(fd, STDERR_FILENO);
      } else {
        try {
          std::cout << pid << std::endl;
          _exit(0);
        } catch (...) {}
        _exit(1);
      }
    }
    result = run(args[0], args[1], args[2], opts);
  } catch (const std::exception& e) {
    vaporoid::system_log(LOG_ERROR, e.what());
  } catch (...) {
    vaporoid::system_log(LOG_ERROR, "Unknown exception");
  }
  closelog();
  return result;
}
