#ifndef VAPOROID_FILE_CONTROL_HPP
#define VAPOROID_FILE_CONTROL_HPP

#include <fcntl.h>
#include "system_error.hpp"

namespace vaporoid {
  namespace file_control {
    inline int get(int fd, int get_command) {
      int flags = fcntl(fd, get_command);
      if (flags == -1) {
        throw system_error("Could not fcntl");
      }
      return flags;
    }

    inline void set(int fd, int get_command, int set_command, int flag, bool value) {
      int flags = get(fd, get_command);
      if (value) {
        flags |= flag;
      } else {
        flags &= ~flag;
      }
      if (fcntl(fd, set_command, flags) == -1) {
        throw system_error("Could not fcntl");
      }
    }

    void set_cloexec(int fd, bool value = true) {
      return set(fd, F_GETFD, F_SETFD, FD_CLOEXEC, value);
    }

    void unset_cloexec(int fd) {
      set_cloexec(fd, false);
    }

    void set_nonblock(int fd, bool value = true) {
      return set(fd, F_GETFL, F_SETFL, O_NONBLOCK, value);
    }

    void unset_nonblock(int fd) {
      set_nonblock(fd, false);
    }
  }
}

#endif
