#ifndef VAPOROID_FILE_FORMAT_HPP
#define VAPOROID_FILE_FORMAT_HPP

#include <iostream>
#include "file_handle.hpp"
#include "print.hpp"
#include "read.hpp"
#include "stdint.hpp"

namespace vaporoid {
  namespace file_format {
    namespace elf {
      struct header {
        uint8_t  ident[16];
        uint16_t type;
        uint16_t machine;
        uint32_t version;
      };

      template <bool T_is_big_endian>
      inline bool read_(file_handle& handle, header& header) {
        header.type    = read_word<uint16_t, T_is_big_endian>(handle);
        header.machine = read_word<uint16_t, T_is_big_endian>(handle);
        header.version = read_word<uint32_t, T_is_big_endian>(handle);
        return true;
      }

      inline bool read(file_handle& handle, header& header) {
        static const uint8_t MAGIC[] = { 0x7F, 'E', 'L', 'F' };
        try {
          read_byte(handle, header.ident, 16);
          if (std::equal(header.ident, header.ident + 4, MAGIC)) {
            // bool is_msb = header.ident[5] == 2
            if (header.ident[5] == 2) {
              return read_<true>(handle, header);
            } else {
              return read_<false>(handle, header);
            }
          }
        } catch (const unexpected_eof& e) {}
        return false;
      }

      inline void print_ident_class(std::ostream& out, const uint8_t* ident) {
        switch (ident[4]) {
          case 0: print_lua_quote(out, "ELFCLASSNONE"); return;
          case 1: print_lua_quote(out, "ELFCLASS32");   return;
          case 2: print_lua_quote(out, "ELFCLASS64");   return;
        }
        out << static_cast<uint16_t>(ident[4]);
      }

      inline void print_ident_data(std::ostream& out, const uint8_t* ident) {
        switch (ident[5]) {
          case 0: print_lua_quote(out, "ELFDATANONE"); return;
          case 1: print_lua_quote(out, "ELFDATA2LSB"); return;
          case 2: print_lua_quote(out, "ELFDATA2MSB"); return;
        }
        out << static_cast<uint16_t>(ident[5]);
      }

      inline void print_version(std::ostream& out, uint32_t version) {
        switch (version) {
          case 0: print_lua_quote(out, "EV_NONE");    return;
          case 1: print_lua_quote(out, "EV_CURRENT"); return;
        }
        out << version;
      }

      inline void print_ident_version(std::ostream& out, const uint8_t* ident) {
        print_version(out, ident[6]);
      }

      inline void print_ident_osabi(std::ostream& out, const uint8_t* ident) {
        switch (ident[7]) {
          case 0: print_lua_quote(out, "ELFOSABI_NONE"   ); return;
          case 6: print_lua_quote(out, "ELFOSABI_SOLARIS"); return;
        }
        out << static_cast<uint16_t>(ident[7]);
      }

      inline void print_ident_abiversion(std::ostream& out, const uint8_t* ident) {
        out << static_cast<uint16_t>(ident[8]);
      }

      inline void print_type(std::ostream& out, uint16_t type) {
        switch (type) {
          case      0: print_lua_quote(out, "ET_NONE");   return;
          case      1: print_lua_quote(out, "ET_REL");    return;
          case      2: print_lua_quote(out, "ET_EXEC");   return;
          case      3: print_lua_quote(out, "ET_DYN");    return;
          case      4: print_lua_quote(out, "ET_CORE");   return;
          case 0xfe00: print_lua_quote(out, "ET_LOOS");   return;
          case 0xfeff: print_lua_quote(out, "ET_HIOS");   return;
          case 0xff00: print_lua_quote(out, "ET_LOPROC"); return;
          case 0xffff: print_lua_quote(out, "ET_HIPROC"); return;
        }
        out << type;
      }

      inline void print_machine(std::ostream& out, uint32_t machine) {
        switch (machine) {
          case  0: print_lua_quote(out, "EM_NONE");        return;
          case  1: print_lua_quote(out, "EM_M32");         return;
          case  2: print_lua_quote(out, "EM_SPARC");       return;
          case  3: print_lua_quote(out, "EM_386");         return;
          case  4: print_lua_quote(out, "EM_68K");         return;
          case  5: print_lua_quote(out, "EM_88K");         return;
          case  7: print_lua_quote(out, "EM_860");         return;
          case  8: print_lua_quote(out, "EM_MIPS");        return;
          case 10: print_lua_quote(out, "EM_MIPS_RS4_BE"); return;
          case 62: print_lua_quote(out, "EM_X86_64");      return;
        }
        out << machine;
      }
    }

    namespace mach_o {
      struct header {
        uint32_t magic;
        uint32_t cputype;
        uint32_t cpusubtype;
        uint32_t filetype;
        uint32_t ncmds;
        uint32_t sizeofcmds;
        uint32_t flags;
      };

      struct fat_header {
        uint32_t magic;
        uint32_t nfat_arch;
      };

      struct fat_arch {
        uint32_t cputype;
        uint32_t cpusubtype;
        uint32_t offset;
        uint32_t size;
        uint32_t align;
      };

      template <bool T_is_big_endian>
      inline bool read_(file_handle& handle, header& header) {
        header.cputype    = read_word<uint32_t, T_is_big_endian>(handle);
        header.cpusubtype = read_word<uint32_t, T_is_big_endian>(handle);
        header.filetype   = read_word<uint32_t, T_is_big_endian>(handle);
        header.ncmds      = read_word<uint32_t, T_is_big_endian>(handle);
        header.sizeofcmds = read_word<uint32_t, T_is_big_endian>(handle);
        header.flags      = read_word<uint32_t, T_is_big_endian>(handle);
        return true;
      }

      inline bool read(file_handle& handle, header& header) {
        try {
          header.magic = read_word<uint32_t>(handle);
          if (header.magic == 0xfeedface || header.magic == 0xfeedfacf) {
            return read_<true>(handle, header);
          } if (header.magic == 0xcefaedfe || header.magic == 0xcffaedfe) {
            return read_<false>(handle, header);
          }
        } catch (const unexpected_eof& e) {}
        return false;
      }

      inline bool read(file_handle& handle, fat_header& fat_header) {
        try {
          fat_header.magic = read_word<uint32_t>(handle);
          if (fat_header.magic == 0xcafebabe) {
            fat_header.nfat_arch = read_word<uint32_t>(handle);
            return (fat_header.nfat_arch & 0xffff) < 0x2d;
            // uint16_t java_class_minor_version = fat_header.nfat_arch >> 16;
            // uint16_t java_class_major_version = fat_header.nfat_arch & 0xffff;
            // return java_class_major_version < 0x2d;
          }
        } catch (const unexpected_eof& e) {}
        return false;
      }

      inline void read(file_handle& handle, fat_arch& fat_arch) {
        fat_arch.cputype    = read_word<uint32_t>(handle);
        fat_arch.cpusubtype = read_word<uint32_t>(handle);
        fat_arch.offset     = read_word<uint32_t>(handle);
        fat_arch.size       = read_word<uint32_t>(handle);
        fat_arch.align      = read_word<uint32_t>(handle);
      }

      inline void print_cputype(std::ostream& out, uint32_t cputype) {
        uint32_t caps = cputype >> 24;
        switch (cputype & 0x00ffffff) {
          case  7: print_lua_quote(out, caps & 0x01 ? "CPU_TYPE_X86_64"    : "CPU_TYPE_X86");     return;
          case 18: print_lua_quote(out, caps & 0x01 ? "CPU_TYPE_POWERPC64" : "CPU_TYPE_POWERPC"); return;

          case  1: print_lua_quote(out, "CPU_TYPE_VAX");     return;
          case  6: print_lua_quote(out, "CPU_TYPE_MC680x0"); return;
          case 10: print_lua_quote(out, "CPU_TYPE_MC98000"); return;
          case 11: print_lua_quote(out, "CPU_TYPE_HPPA");    return;
          case 12: print_lua_quote(out, "CPU_TYPE_ARM");     return;
          case 13: print_lua_quote(out, "CPU_TYPE_MC88000"); return;
          case 14: print_lua_quote(out, "CPU_TYPE_SPARC");   return;
          case 15: print_lua_quote(out, "CPU_TYPE_I860");    return;
        }
        out << cputype;
      }

      inline void print_cpusubtype(std::ostream& out, uint32_t cputype, uint32_t cpusubtype) {
        uint32_t caps = cpusubtype >> 24;
        switch (cputype & 0x00ffffff) {
          case 7:
            switch (cpusubtype & 0x00ffffff) {
              case 3: print_lua_quote(out, caps & 0x80 ? "CPU_SUBTYPE_X86_64_ALL" : "CPU_SUBTYPE_X86_ALL"); return;
            }
            break;
          case 18:
            switch (cpusubtype & 0x00ffffff) {
              case   0: print_lua_quote(out, "CPU_SUBTYPE_POWERPC_ALL"  ); return;
              case   1: print_lua_quote(out, "CPU_SUBTYPE_POWERPC_601"  ); return;
              case   2: print_lua_quote(out, "CPU_SUBTYPE_POWERPC_602"  ); return;
              case   3: print_lua_quote(out, "CPU_SUBTYPE_POWERPC_603"  ); return;
              case   4: print_lua_quote(out, "CPU_SUBTYPE_POWERPC_603e" ); return;
              case   5: print_lua_quote(out, "CPU_SUBTYPE_POWERPC_603ev"); return;
              case   6: print_lua_quote(out, "CPU_SUBTYPE_POWERPC_604"  ); return;
              case   7: print_lua_quote(out, "CPU_SUBTYPE_POWERPC_604e" ); return;
              case   8: print_lua_quote(out, "CPU_SUBTYPE_POWERPC_620"  ); return;
              case   9: print_lua_quote(out, "CPU_SUBTYPE_POWERPC_750"  ); return;
              case  10: print_lua_quote(out, "CPU_SUBTYPE_POWERPC_7400" ); return;
              case  11: print_lua_quote(out, "CPU_SUBTYPE_POWERPC_7450" ); return;
              case 100: print_lua_quote(out, "CPU_SUBTYPE_POWERPC_970"  ); return;
            }
            break;
        }
        out << cpusubtype;
      }

      inline void print_filetype(std::ostream& out, uint32_t filetype) {
        switch (filetype) {
          case 0x01: print_lua_quote(out, "MH_OBJECT");      return;
          case 0x02: print_lua_quote(out, "MH_EXECUTE");     return;
          case 0x03: print_lua_quote(out, "MH_FVMLIB");      return;
          case 0x04: print_lua_quote(out, "MH_CORE");        return;
          case 0x05: print_lua_quote(out, "MH_PRELOAD");     return;
          case 0x06: print_lua_quote(out, "MH_DYLIB");       return;
          case 0x07: print_lua_quote(out, "MH_DYLINKER");    return;
          case 0x08: print_lua_quote(out, "MH_BUNDLE");      return;
          case 0x09: print_lua_quote(out, "MH_DYLIB_STUB");  return;
          case 0x0a: print_lua_quote(out, "MH_DSYM");        return;
          case 0x0b: print_lua_quote(out, "MH_KEXT_BUNDLE"); return;
        }
        out << filetype;
      }
    }
  }
}

#endif
