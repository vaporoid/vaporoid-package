#ifndef VAPOROID_PIPE_HPP
#define VAPOROID_PIPE_HPP

#include <unistd.h>
#include "system_error.hpp"

namespace vaporoid {
  inline void pipe(int& reader, int& writer) {
    int fd[2] = { -1, -1 };
    if (::pipe(fd) == -1) {
      throw system_error("Could not pipe");
    }
    reader = fd[0];
    writer = fd[1];
  }
}

#endif
