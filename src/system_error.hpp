#ifndef VAPOROID_SYSTEM_ERROR_HPP
#define VAPOROID_SYSTEM_ERROR_HPP

#include <errno.h>
#include <stddef.h>
#include <string.h>
#include <sstream>
#include <stdexcept>
#include <vector>

namespace vaporoid {
  namespace system_error_detail {
    inline const char* impl_strerror_r_result(const char*, const char* result) {
      return result;
    }

    inline const char* impl_strerror_r_result(const char* data, int result) {
      if (result == 0) {
        return data;
      }
      if (result != -1) {
        errno = result;
      }
      return 0;
    }

    inline const char* impl_strerror_r(int code, char* data, size_t size) {
      return impl_strerror_r_result(data, strerror_r(code, data, size));
    }

    template <size_t T_size>
    class system_error : public std::runtime_error {
    public:
      explicit system_error(const std::string& what, int code = errno)
        : std::runtime_error(what), code_(code) {}

      virtual ~system_error() throw() {}

      int code() const {
        return code_;
      }

      virtual const char* what() const throw() {
        try {
          if (what_.empty()) {
            std::ostringstream out;
            out << std::runtime_error::what() << ": ";

            char static_buffer[T_size];
            errno = 0;
            const char* message = impl_strerror_r(code_, static_buffer, T_size);

            if (message && *message) {
              out << message;
            } else if (errno != ERANGE) {
              out << "error number: " << code_;
            } else {
              std::vector<char> dynamic_buffer(T_size * 2);
              while (true) {
                errno = 0;
                const char* message = impl_strerror_r(code_, &dynamic_buffer[0], dynamic_buffer.size());

                if (message && *message) {
                  out << message;
                  break;
                } else if (errno != ERANGE) {
                  out << "error number: " << code_;
                  break;
                } else {
                  dynamic_buffer.resize(dynamic_buffer.size() * 2);
                }
              }
            }

            what_ = out.str();
          }
          return what_.c_str();
        } catch (...) {
          return std::runtime_error::what();
        }
      }

    private:
      int code_;
      mutable std::string what_;
    };
  }

  typedef system_error_detail::system_error<128> system_error;
}

#endif
