#ifndef VAPOROID_SCOPED_SIGNAL_ACTION_HPP
#define VAPOROID_SCOPED_SIGNAL_ACTION_HPP

#include <signal.h>
#include <boost/noncopyable.hpp>
#include "system_error.hpp"

namespace vaporoid {
  namespace scoped_signal_action_detail {
    class scoped_signal_action : boost::noncopyable {
    public:
      typedef void (*handler_type)(int);

      explicit scoped_signal_action(int sig, handler_type handler)
        : sig_(sig) {
        struct sigaction sa = {};
        sa.sa_handler = handler;
        if (sigaction(sig, &sa, &sa_) == -1) {
          throw system_error("Could not sigaction");
        }
      }

      ~scoped_signal_action() {
        sigaction(sig_, &sa_, 0);
      }

    private:
      int sig_;
      struct sigaction sa_;
    };
  }

  using scoped_signal_action_detail::scoped_signal_action;
}

#endif
