#ifndef VAPOROID_HASH_HPP
#define VAPOROID_HASH_HPP

#include <stddef.h>
#include <string>
#include <boost/noncopyable.hpp>
#include <boost/scoped_ptr.hpp>
#include "sha.hpp"
#include "stdint.hpp"

namespace vaporoid {
  namespace hash_detail {
    class context {
    public:
      virtual ~context() {}
      virtual size_t message_digest_size_bits() const = 0;
      virtual size_t message_digest_size() const = 0;
      virtual void reset() = 0;
      virtual void update(const uint8_t* i, const uint8_t* end) = 0;
      virtual void update(const void* data, size_t size) = 0;
      virtual uint8_t* finalize(uint8_t* i) = 0;
    };

    template <typename T>
    class context_impl : public context, boost::noncopyable {
    public:
      size_t message_digest_size_bits() const {
        return T::MESSAGE_DIGEST_SIZE_BITS;
      }

      size_t message_digest_size() const {
        return T::MESSAGE_DIGEST_SIZE;
      }

      void reset() {
        ctx_.reset();
      }

      void update(const uint8_t* i, const uint8_t* end) {
        ctx_.update(i, end);
      }

      void update(const void* data, size_t size) {
        ctx_.update(data, size);
      }

      uint8_t* finalize(uint8_t* i) {
        return ctx_.finalize(i);
      }

    private:
      T ctx_;
    };

    class hash {
    public:
      explicit hash(const std::string& algorithm) {
        if (algorithm == "sha1") {
          ctx_.reset(new context_impl<sha1>());
        } else if (algorithm == "sha224") {
          ctx_.reset(new context_impl<sha224>());
        } else if (algorithm == "sha256") {
          ctx_.reset(new context_impl<sha256>());
        } else if (algorithm == "sha384") {
          ctx_.reset(new context_impl<sha384>());
        } else if (algorithm == "sha512") {
          ctx_.reset(new context_impl<sha512>());
        }
      }

      size_t message_digest_size_bits() const {
        return ctx_->message_digest_size_bits();
      }

      size_t message_digest_size() const {
        return ctx_->message_digest_size();
      }

      void reset() {
        ctx_->reset();
      }

      void update(const uint8_t* i, const uint8_t* end) {
        ctx_->update(i, end);
      }

      void update(const void* data, size_t size) {
        ctx_->update(data, size);
      }

      uint8_t* finalize(uint8_t* i) {
        return ctx_->finalize(i);
      }

    private:
      boost::scoped_ptr<context> ctx_;
    };
  }

  using hash_detail::hash;
}

#endif
