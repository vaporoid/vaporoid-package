#ifndef VAPOROID_READ_HPP
#define VAPOROID_READ_HPP

#include "file_handle.hpp"
#include "stdint.hpp"
#include "unexpected_eof.hpp"
#include "word.hpp"

namespace vaporoid {
  namespace read_detail {
    inline void read_byte(file_handle& reader, uint8_t* data, size_t size) {
      if (reader.read(data, size) < size) {
        throw unexpected_eof();
      }
    }

    template <typename T, bool T_is_big_endian>
    struct word_reader {
      static T read(file_handle& reader) {
        uint8_t data[sizeof(T)];
        read_byte(reader, data, sizeof(T));
        return byte_to_word<T, T_is_big_endian>(data);
      }
    };

    template <typename T>
    inline T read_word(file_handle& reader) {
      return word_reader<T, true>::read(reader);
    }

    template <bool T_is_big_endian, typename T>
    inline T read_word(file_handle& reader) {
      return word_reader<T, T_is_big_endian>::read(reader);
    }

    template <typename T, bool T_is_big_endian>
    inline T read_word(file_handle& reader) {
      return word_reader<T, T_is_big_endian>::read(reader);
    }

    template <typename T>
    inline T read_word(file_handle& reader, bool is_big_endian) {
      return is_big_endian
        ? word_reader<T, true> ::read(reader)
        : word_reader<T, false>::read(reader);
    }
  }

  using read_detail::read_byte;
  using read_detail::read_word;
}

#endif
