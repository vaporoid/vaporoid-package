#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <exception>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>
#include <boost/foreach.hpp>
#include <boost/io/ios_state.hpp>
#include "directory_handle.hpp"
#include "file_format.hpp"
#include "file_handle.hpp"
#include "hash.hpp"
#include "print.hpp"
#include "stdint.hpp"
#include "system_error.hpp"

namespace {
  using vaporoid::uint8_t;
  using vaporoid::uint32_t;

  struct options {
    bool atime;
    bool binary;
    std::vector<std::string> digest;
  };

  inline void print_help(std::ostream& out, const char* program) {
    out << "SYNOPSIS\n"
        << "  " << program << " [-a] [-b] [-d <algorithm>] [directory]\n"
        << "  " << program << " -h\n"
        << "\n"
        << "OPTIONS\n"
        << "  -a              Print the last data access timestamp.\n"
        << "  -b              Print the ELF and Mach-O binary object header.\n"
        << "  -d <algorithm>  Print the FIPS 180-3 SHS message digest.\n"
        << "                  (sha1|sha224|sha256|sha384|sha512)\n"
        << "  -h              Print a description of the command line options.\n"
        << "\n"
        << "OPERANDS\n"
        << "  directory       The root directory.\n"
        << "                  (Default: the current directory)\n"
        << "\n";
  }

  inline void print_elf(std::ostream& out, vaporoid::file_handle& handle) {
    handle.seek(0);
    vaporoid::file_format::elf::header header = {};
    if (vaporoid::file_format::elf::read(handle, header)) {
      out << "    elf = {\n"
          << "      ident = {\n"
          << "        class = ";      vaporoid::file_format::elf::print_ident_class     (out, header.ident); out << ";\n"
          << "        data = ";       vaporoid::file_format::elf::print_ident_data      (out, header.ident); out << ";\n"
          << "        version = ";    vaporoid::file_format::elf::print_ident_version   (out, header.ident); out << ";\n"
          << "        osabi = ";      vaporoid::file_format::elf::print_ident_osabi     (out, header.ident); out << ";\n"
          << "        abiversion = "; vaporoid::file_format::elf::print_ident_abiversion(out, header.ident); out << ";\n"
          << "      };\n"
          << "      type = ";    vaporoid::file_format::elf::print_type   (out, header.type);    out << ";\n"
          << "      machine = "; vaporoid::file_format::elf::print_machine(out, header.machine); out << ";\n"
          << "      version = "; vaporoid::file_format::elf::print_version(out, header.version); out << ";\n"
          << "    };\n";
    }
  }

  inline void print_mach_o(std::ostream& out, vaporoid::file_handle& handle) {
    handle.seek(0);
    vaporoid::file_format::mach_o::header header = {};
    if (vaporoid::file_format::mach_o::read(handle, header)) {
      out << "    mach_o = {\n"
          << "      cputype = ";    vaporoid::file_format::mach_o::print_cputype   (out, header.cputype);                    out << ";\n"
          << "      cpusubyupe = "; vaporoid::file_format::mach_o::print_cpusubtype(out, header.cputype, header.cpusubtype); out << ";\n"
          << "      filetype = ";   vaporoid::file_format::mach_o::print_filetype  (out, header.filetype);                   out << ";\n"
          << "      ncmds = "      << header.ncmds      << ";\n"
          << "      sizeofcmds = " << header.sizeofcmds << ";\n"
          << "      flags = "      << header.flags      << ";\n"
          << "    };\n";
      return;
    }

    handle.seek(0);
    vaporoid::file_format::mach_o::fat_header fat_header = {};
    if (vaporoid::file_format::mach_o::read(handle, fat_header)) {
      out << "    mach_o = {\n"
          << "      nfat_arch = " << fat_header.nfat_arch << ";\n"
          << "      fat_arch = {\n";
      for (uint32_t i = 0; i < fat_header.nfat_arch; ++i) {
        vaporoid::file_format::mach_o::fat_arch fat_arch = {};
        vaporoid::file_format::mach_o::read(handle, fat_arch);

        long save = handle.tell();
        vaporoid::file_format::mach_o::header header = {};
        handle.seek(fat_arch.offset);
        vaporoid::file_format::mach_o::read(handle, header);
        handle.seek(save);

        out << "        {\n"
            << "          cputype = ";    vaporoid::file_format::mach_o::print_cputype   (out, fat_arch.cputype);                      out << ";\n"
            << "          cpusubyupe = "; vaporoid::file_format::mach_o::print_cpusubtype(out, fat_arch.cputype, fat_arch.cpusubtype); out << ";\n"
            << "          offset = " << fat_arch.offset << ";\n"
            << "          size = "   << fat_arch.size   << ";\n"
            << "          align = "  << fat_arch.align  << ";\n"
            << "          header = {\n"
            << "            cputype = ";    vaporoid::file_format::mach_o::print_cputype   (out, header.cputype);                    out << ";\n"
            << "            cpusubyupe = "; vaporoid::file_format::mach_o::print_cpusubtype(out, header.cputype, header.cpusubtype); out << ";\n"
            << "            filetype = ";   vaporoid::file_format::mach_o::print_filetype  (out, header.filetype);                   out << ";\n"
            << "            ncmds = "      << header.ncmds      << ";\n"
            << "            sizeofcmds = " << header.sizeofcmds << ";\n"
            << "            flags = "      << header.flags      << ";\n"
            << "          };\n"
            << "        };\n";
      }
      out << "      };\n"
          << "    };\n";
    }
  }

  inline void print_digest(std::ostream& out, const std::string& algorithm, vaporoid::file_handle& handle) {
    vaporoid::hash hash(algorithm);
    std::vector<char> buffer(4096);

    handle.seek(0);
    while (true) {
      size_t read_size = handle.read(&buffer[0], buffer.size());
      if (read_size > 0) {
        hash.update(&buffer[0], read_size);
      }
      if (read_size < buffer.size()) {
        break;
      }
    }

    std::vector<uint8_t> md(hash.message_digest_size());
    hash.finalize(&md[0]);
    out << "    " << algorithm << " = \""; vaporoid::print_hex(out, md.begin(), md.end()); out << "\";\n";
  }

  inline void run(std::ostream& out, const std::string& dirname, const options& opts) {
    long path_max = -1;
    vaporoid::directory_handle directory_handle(dirname.c_str());

    std::string d_name;
    while (const char* d_name = directory_handle.read()) {
      std::string name = d_name;
      if (name == "." || name == "..") {
        continue;
      }
      std::string filename = dirname + "/" + name;

      out << "  {\n"
          << "    filename = "; vaporoid::print_lua_quote(out, filename); out << ";\n";
      struct stat status = {};
      if (lstat(filename.c_str(), &status) == -1) {
        throw vaporoid::system_error("Could not lstat");
      }
      out << "    dev = " << status.st_dev << ";\n"
          << "    ino = " << status.st_ino << ";\n"
          << "    type = \"";
      if (S_ISBLK(status.st_mode)) {
        out << "block";
      } else if (S_ISCHR(status.st_mode)) {
        out << "char";
      } else if (S_ISDIR(status.st_mode)) {
        out << "dir";
      } else if (S_ISFIFO(status.st_mode)) {
        out << "fifo";
      } else if (S_ISREG(status.st_mode)) {
        out << "file";
      } else if (S_ISLNK(status.st_mode)) {
        out << "link";
      } else if (S_ISSOCK(status.st_mode)) {
        out << "socket";
      }
      out << "\";\n"
          << "    mode = \""; { boost::io::ios_all_saver save(out); out << std::oct << std::setfill('0') << std::setw(4) << (status.st_mode & 07777); } out << "\";\n"
          << "    nlink = "   << status.st_nlink << ";\n"
          << "    uid = "     << status.st_uid   << ";\n"
          << "    gid = "     << status.st_gid   << ";\n"
          << "    size = "    << status.st_size  << ";\n";
      if (opts.atime) {
        out << "    atime = " << status.st_atime << ";\n";
      }
      out << "    mtime = "   << status.st_mtime   << ";\n"
          << "    ctime = "   << status.st_ctime   << ";\n"
          << "    blksize = " << status.st_blksize << ";\n"
          << "    blocks = "  << status.st_blocks  << ";\n";

      if (S_ISREG(status.st_mode) && (opts.binary || !opts.digest.empty())) {
        vaporoid::file_handle handle(filename.c_str(), "rb");

        if (opts.binary) {
          vaporoid::file_handle handle(filename.c_str(), "rb");
          print_elf   (out, handle);
          print_mach_o(out, handle);
        }

        BOOST_FOREACH(const std::string& algorithm, opts.digest) {
          print_digest(out, algorithm, handle);
        }
      }

      if (S_ISLNK(status.st_mode)) {
        if (path_max == -1) {
          path_max = pathconf(dirname.c_str(), _PC_PATH_MAX);
          if (path_max == -1) {
            throw vaporoid::system_error("Could not pathconf");
          }
        }
        std::vector<char> data(path_max);
        ssize_t size = readlink(filename.c_str(), &data[0], data.size());
        if (size == -1) {
          throw vaporoid::system_error("Could not readlink");
        }
        out << "    link = "; vaporoid::print_lua_quote(out, &data[0], &data[0] + size); out << ";\n";
        if (!realpath(filename.c_str(), &data[0])) {
          throw vaporoid::system_error("Could not realpath");
        }
        out << "    realpath = "; vaporoid::print_lua_quote(out, &data[0]); out << ";\n";
      }

      out << "  };\n";

      if (S_ISDIR(status.st_mode)) {
        run(out, filename, opts);
      }
    }
  }
}

extern int   optind;
extern char* optarg;

int main(int argc, char* argv[]) {
  try {
    options opts = {};
    while (true) {
      int c = getopt(argc, argv, "abd:h");
      if (c == -1) {
        break;
      }
      switch (c) {
        case 'a':
          opts.atime = true;
          break;
        case 'b':
          opts.binary = true;
          break;
        case 'd':
          {
            std::string algorithm = optarg;
            if (algorithm == "sha1" || algorithm == "sha224" || algorithm == "sha256" || algorithm == "sha384" || algorithm == "sha512") {
              opts.digest.push_back(algorithm);
            } else {
              print_help(std::cout, argv[0]);
              return 1;
            }
          }
          break;
        case 'h':
          print_help(std::cout, argv[0]);
          return 0;
        case '?':
          print_help(std::cout, argv[0]);
          return 1;
      }
    }

    if (optind < argc) {
      if (chdir(argv[optind]) == -1) {
        throw vaporoid::system_error("Could not chdir");
      }
    }

    std::cout << "return {\n";
    run(std::cout, ".", opts);
    std::cout << "}\n";
    return 0;
  } catch (const std::exception& e) {
    std::cerr << e.what() << "\n";
  } catch (...) {
    std::cerr << "Unknown exception\n";
  }
  return 1;
}
