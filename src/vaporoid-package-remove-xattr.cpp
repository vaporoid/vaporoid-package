#if defined(__APPLE__) && defined(__MACH__)

#include <string.h>
#include <sys/xattr.h>
#include <exception>
#include <iostream>
#include <vector>
#include "system_error.hpp"

namespace {
  inline void print_help(std::ostream& out, const char* program) {
    out << "Usage: " << program << " FILE...\n";
  }

  inline void remove_xattr(std::ostream&, const char* filename) {
    ssize_t size = listxattr(filename, 0, 0, 0);
    if (size == -1) {
      throw vaporoid::system_error("Could not listxattr");
    }
    if (size == 0) {
      return;
    }

    std::vector<char> name(size);
    size = listxattr(filename, &name[0], size, 0);
    if (size == -1) {
      throw vaporoid::system_error("Could not listxattr");
    }
    if (size == 0) {
      return;
    }

    for (const char* i = &name[0]; i - &name[0] < size; i += strlen(i) + 1) {
      if (removexattr(filename, i, 0) == -1) {
        throw vaporoid::system_error("Could not removexattr");
      }
    }
  }
}

int main(int argc, char* argv[]) {
  try {
    if (argc < 2) {
      print_help(std::cout, argv[0]);
      return 1;
    }
    for (int i = 1; i < argc; ++i) {
      remove_xattr(std::cout, argv[i]);
    }
    return 0;
  } catch (const std::exception& e) {
    std::cerr << e.what() << "\n";
  } catch (...) {
    std::cerr << "Unknown exception\n";
  }
  return 1;
}

#else

#include <iostream>

int main(int, char*[]) {
  std::cerr << "Not implemented\n";
  return 1;
}

#endif
