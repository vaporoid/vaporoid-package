#ifndef VAPOROID_EXECUTE_HPP
#define VAPOROID_EXECUTE_HPP

#include <errno.h>
#include <paths.h>
#include <stddef.h>
#include <unistd.h>
#include <algorithm>
#include <string>
#include <vector>
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include "system_error.hpp"

extern char** environ;

namespace vaporoid {
  namespace execute_detail {
    class argument_vector {
    public:
      explicit argument_vector(const std::vector<std::string>& args)
        : vector_(args.size() + 1) {
        size_t size = vector_.size() + 1;
        BOOST_FOREACH(const std::string& i, args) {
          size += i.size();
        }
        buffer_.resize(size);

        char** v = &vector_[0];
        char*  b = &buffer_[0];

        BOOST_FOREACH(const std::string& i, args) {
          *v++ = b;
          b = std::copy(i.begin(), i.end(), b);
          *b++ = 0;
        }

        *v = 0;
        *b = 0;
      }

      char* const* get() {
        return &vector_[0];
      }

    private:
      std::vector<char*> vector_;
      std::vector<char>  buffer_;
    };

    inline std::string getenv(char* const envp[], std::string name) {
      name += "=";
      for (const char* const* i = envp; *i; ++i) {
        std::string value = *i;
        if (value.find(name) == 0) {
          return value.substr(name.size());
        }
      }
#ifdef _PATH_STDPATH
      return _PATH_STDPATH;
#else
      return "/usr/bin:/bin:/usr/sbin:/sbin";
#endif
    }

    inline void execute(const std::string& program, char* const argv[], char* const envp[]) {
      if (program.find("/") != std::string::npos) {
        execve(program.c_str(), argv, envp);
        throw system_error("Could not execve");
      }

      int code = 0;
      std::string path = getenv(envp, "PATH");
      boost::tokenizer<boost::char_separator<char> > token(path, boost::char_separator<char>(":", "", boost::keep_empty_tokens));
      BOOST_FOREACH(std::string i, token) {
        if (i.empty()) {
          i = ".";
        }
        i += "/";
        i += program;

        execve(i.c_str(), argv, envp);
        code = errno;
        if (code != EACCES && code != EISDIR && code != ENOENT && code != EPERM) {
          break;
        }
      }
      throw system_error("Could not execve", code);
    }

    inline void execute(const std::vector<std::string>& args) {
      argument_vector argv(args);
      execute(args.front(), argv.get(), environ);
    }
  }

  using execute_detail::execute;
}

#endif
