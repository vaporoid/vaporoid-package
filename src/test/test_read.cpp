#include "read.hpp"
#include <unistd.h>
#include <string>
#include "file_handle.hpp"
#include "stdint.hpp"
#include "test.hpp"

namespace {
  using vaporoid::uint8_t;
  using vaporoid::uint16_t;
  using vaporoid::uint32_t;
  using vaporoid::uint64_t;

  template <typename T>
  inline void test(const std::string& filename, T be_word, T le_word, const uint8_t* byte) {
    {
      vaporoid::file_handle h(filename.c_str(), "wb");
      for (int i = 0; i < 16; ++i) {
        h.write(byte, sizeof(T));
      }
    }

    {
      vaporoid::file_handle h(filename.c_str(), "rb");
      BOOST_CHECK((vaporoid::read_word<T>       (h)        == be_word));
      BOOST_CHECK((vaporoid::read_word<true,  T>(h)        == be_word));
      BOOST_CHECK((vaporoid::read_word<false, T>(h)        == le_word));
      BOOST_CHECK((vaporoid::read_word<T, true> (h)        == be_word));
      BOOST_CHECK((vaporoid::read_word<T, false>(h)        == le_word));
      BOOST_CHECK((vaporoid::read_word<T>       (h, true)  == be_word));
      BOOST_CHECK((vaporoid::read_word<T>       (h, false) == le_word));
    }
  }

  inline void test_uint16(const std::string& filename) {
    static const uint8_t byte[] = { 0x01, 0x02 };
    test<uint16_t>(filename, 0x0102, 0x0201, byte);
  }

  inline void test_uint32(const std::string& filename) {
    static const uint8_t byte[] = { 0x01, 0x02, 0x03, 0x04 };
    test<uint32_t>(filename, 0x01020304, 0x04030201, byte);
  }

  inline void test_uint64(const std::string& filename) {
    static const uint8_t byte[] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08 };
    test<uint64_t>(filename, 0x0102030405060708ULL, 0x0807060504030201ULL, byte);
  }
}

int test_main(int, char* argv[]) {
  std::string filename = std::string(argv[0]) + ".tmp";
  test_uint16(filename);
  test_uint32(filename);
  test_uint64(filename);
  unlink(filename.c_str());
  return 0;
}
