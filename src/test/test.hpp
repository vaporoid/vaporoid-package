#ifndef VAPOROID_PACKAGE_SRC_TEST_TEST_HPP
#define VAPOROID_PACKAGE_SRC_TEST_TEST_HPP

#include <signal.h>

#ifdef __OpenBSD__
# ifndef SI_ASYNCIO
#  define SI_ASYNCIO (-4)
# endif
# ifndef SI_MESGQ
#  define SI_MESGQ (-5)
# endif
#endif

#if defined(__GNUC__) && defined(__GNUC_MINOR__)
# if (__GNUC__ == 4 && __GNUC_MINOR__ >= 6) || __GNUC__ > 4
#  pragma GCC diagnostic push
#  pragma GCC diagnostic ignored "-Wunused-parameter"
# endif
#endif

#include <boost/test/minimal.hpp>

#if defined(__GNUC__) && defined(__GNUC_MINOR__)
# if (__GNUC__ == 4 && __GNUC_MINOR__ >= 6) || __GNUC__ > 4
#  pragma GCC diagnostic pop
# endif
#endif

#endif
