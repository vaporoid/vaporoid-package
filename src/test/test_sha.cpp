#include "sha.hpp"
#include <stddef.h>
#include <algorithm>
#include <sstream>
#include <string>
#include <iostream>
#include <vector>
#include "print.hpp"
#include "stdint.hpp"
#include "test.hpp"
#include "test_sha.hpp"

namespace {
  using vaporoid::uint8_t;

  template <typename T>
  void test1(const std::string& expected) {
    T sha;
    std::ostringstream out;

    {
      sha.update("foo\n", 4);
      uint8_t md[T::MESSAGE_DIGEST_SIZE] = { 0 };
      sha.finalize(md);
      vaporoid::print_hex(out, md, md + T::MESSAGE_DIGEST_SIZE);
      out << "\n";
    }

    {
      sha.reset();
      sha.update("foo\n", 4);
      uint8_t md[T::MESSAGE_DIGEST_SIZE] = { 0 };
      sha.finalize(md);
      vaporoid::print_hex(out, md, md + T::MESSAGE_DIGEST_SIZE);
      out << "\n";
    }

    {
      sha.reset();
      sha.update("foo\n", 4);
      sha.reset();
      sha.update("foo\n", 4);
      uint8_t md[T::MESSAGE_DIGEST_SIZE] = { 0 };
      sha.finalize(md);
      vaporoid::print_hex(out, md, md + T::MESSAGE_DIGEST_SIZE);
      out << "\n";
    }

    std::cout << out.str();
    BOOST_CHECK(out.str() == expected + "\n" + expected + "\n" + expected + "\n");
  }

  template <typename T>
  inline void test2(const std::vector<uint8_t>& buffer, size_t size, const std::string& expected) {
    T sha;

    while (size > 0) {
      size_t update_size = std::min(buffer.size(), size);
      sha.update(&buffer[0], update_size);
      size -= update_size;
    }

    uint8_t md[T::MESSAGE_DIGEST_SIZE] = { 0 };
    sha.finalize(md);
    std::ostringstream out;
    vaporoid::print_hex(out, md, md + T::MESSAGE_DIGEST_SIZE);

    BOOST_CHECK(out.str() == expected);
  }

  inline void test2(size_t size) {
    std::vector<uint8_t> buffer(size);
    memset(&buffer[0], 'x', buffer.size());

    for (size_t i = 0; i < test_sha::size; ++i) {
      const test_sha::item& item = test_sha::data[i];
      std::string algorithm = item.algorithm;
      if (algorithm == "sha1") {
        test2<vaporoid::sha1>(buffer, item.size, item.md);
      } else if (algorithm == "sha224") {
        test2<vaporoid::sha224>(buffer, item.size, item.md);
      } else if (algorithm == "sha256") {
        test2<vaporoid::sha256>(buffer, item.size, item.md);
      } else if (algorithm == "sha384") {
        test2<vaporoid::sha384>(buffer, item.size, item.md);
      } else if (algorithm == "sha512") {
        test2<vaporoid::sha512>(buffer, item.size, item.md);
      }
    }
  }
}

int test_main(int, char*[]) {
  test1<vaporoid::sha1>  ("f1d2d2f924e986ac86fdf7b36c94bcdf32beec15");
  test1<vaporoid::sha224>("e7d5e36e8d470c3e5103fedd2e4f2aa5c30ab27f6629bdc3286f9dd2");
  test1<vaporoid::sha256>("b5bb9d8014a0f9b1d61e21e796d78dccdf1352f23cd32812f4850b878ae4944c");
  test1<vaporoid::sha384>("8effdabfe14416214a250f935505250bd991f106065d899db6e19bdc8bf648f3ac0f1935c4f65fe8f798289b1a0d1e06");
  test1<vaporoid::sha512>("0cf9180a764aba863a67b6d72f0918bc131c6772642cb2dce5a34f0a702f9470ddc2bf125c12198b1995c233c34b4afd346c54a2334c350a948a51b6e8b4e6b6");

  test2(  17);
  test2( 129);
  test2(4096);

  return 0;
}
