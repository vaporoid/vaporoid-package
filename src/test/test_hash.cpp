#include "hash.hpp"
#include <stddef.h>
#include <algorithm>
#include <sstream>
#include <string>
#include <iostream>
#include <vector>
#include "print.hpp"
#include "stdint.hpp"
#include "test.hpp"
#include "test_sha.hpp"

namespace {
  using vaporoid::uint8_t;

  void test(const std::string& algorithm, const std::string& expected) {
    vaporoid::hash hash(algorithm);
    std::ostringstream out;

    {
      hash.update("foo\n", 4);
      std::vector<uint8_t> md(hash.message_digest_size());
      hash.finalize(&md[0]);
      vaporoid::print_hex(out, md.begin(), md.end());
      out << "\n";
    }

    {
      hash.reset();
      hash.update("foo\n", 4);
      std::vector<uint8_t> md(hash.message_digest_size());
      hash.finalize(&md[0]);
      vaporoid::print_hex(out, md.begin(), md.end());
      out << "\n";
    }

    {
      hash.reset();
      hash.update("foo\n", 4);
      hash.reset();
      hash.update("foo\n", 4);
      std::vector<uint8_t> md(hash.message_digest_size());
      hash.finalize(&md[0]);
      vaporoid::print_hex(out, md.begin(), md.end());
      out << "\n";
    }

    std::cout << out.str();
    BOOST_CHECK(out.str() == expected + "\n" + expected + "\n" + expected + "\n");
  }
}

int test_main(int, char*[]) {
  test("sha1",   "f1d2d2f924e986ac86fdf7b36c94bcdf32beec15");
  test("sha224", "e7d5e36e8d470c3e5103fedd2e4f2aa5c30ab27f6629bdc3286f9dd2");
  test("sha256", "b5bb9d8014a0f9b1d61e21e796d78dccdf1352f23cd32812f4850b878ae4944c");
  test("sha384", "8effdabfe14416214a250f935505250bd991f106065d899db6e19bdc8bf648f3ac0f1935c4f65fe8f798289b1a0d1e06");
  test("sha512", "0cf9180a764aba863a67b6d72f0918bc131c6772642cb2dce5a34f0a702f9470ddc2bf125c12198b1995c233c34b4afd346c54a2334c350a948a51b6e8b4e6b6");
  return 0;
}
