#include "file_control.hpp"
#include <errno.h>
#include <unistd.h>
#include "test.hpp"

int test_main(int, char*[]) {
  vaporoid::file_control::set_cloexec (STDIN_FILENO, true);
  vaporoid::file_control::set_nonblock(STDIN_FILENO, true);

  char data[1];
  errno = 0;
  BOOST_CHECK(read(STDIN_FILENO, data, 1) == -1);
  BOOST_CHECK(errno == EAGAIN || errno == EWOULDBLOCK);

  return 0;
}
