#include "system_log.hpp"
#include <syslog.h>
#include "test.hpp"

int test_main(int, char*[]) {
  vaporoid::system_log(LOG_ERR, "foo");
  vaporoid::system_log(LOG_ERR, "foo", "bar", "baz", 42);
  closelog();
  return 0;
}
