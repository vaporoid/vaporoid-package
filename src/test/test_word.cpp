#include "word.hpp"
#include <algorithm>
#include "stdint.hpp"
#include "test.hpp"

namespace {
  using vaporoid::uint8_t;
  using vaporoid::uint16_t;
  using vaporoid::uint32_t;
  using vaporoid::uint64_t;

  template <typename T>
  inline void test_word_to_byte(const uint8_t* begin, const uint8_t* end, const uint8_t* expected) {
    BOOST_CHECK(begin + sizeof(T) == end);
    BOOST_CHECK(std::equal(begin, end, expected));
  }

  template <typename T>
  inline void test(T be_word, T le_word, const uint8_t* byte) {
    uint8_t buffer[sizeof(T)] = { 0 };

    test_word_to_byte<T>(buffer, vaporoid::word_to_byte          (be_word, buffer),        byte);
    test_word_to_byte<T>(buffer, vaporoid::word_to_byte<true>    (be_word, buffer),        byte);
    test_word_to_byte<T>(buffer, vaporoid::word_to_byte<false>   (le_word, buffer),        byte);
    test_word_to_byte<T>(buffer, vaporoid::word_to_byte<T, true> (be_word, buffer),        byte);
    test_word_to_byte<T>(buffer, vaporoid::word_to_byte<T, false>(le_word, buffer),        byte);
    test_word_to_byte<T>(buffer, vaporoid::word_to_byte          (be_word, buffer, true),  byte);
    test_word_to_byte<T>(buffer, vaporoid::word_to_byte          (le_word, buffer, false), byte);

    BOOST_CHECK((vaporoid::byte_to_word<T>       (byte)        == be_word));
    BOOST_CHECK((vaporoid::byte_to_word<true,  T>(byte)        == be_word));
    BOOST_CHECK((vaporoid::byte_to_word<false, T>(byte)        == le_word));
    BOOST_CHECK((vaporoid::byte_to_word<T, true> (byte)        == be_word));
    BOOST_CHECK((vaporoid::byte_to_word<T, false>(byte)        == le_word));
    BOOST_CHECK((vaporoid::byte_to_word<T>       (byte, true)  == be_word));
    BOOST_CHECK((vaporoid::byte_to_word<T>       (byte, false) == le_word));
  }

  inline void test_uint16() {
    static const uint8_t byte[] = { 0x01, 0x02 };
    test<uint16_t>(0x0102, 0x0201, byte);
  }

  inline void test_uint32() {
    static const uint8_t byte[] = { 0x01, 0x02, 0x03, 0x04 };
    test<uint32_t>(0x01020304, 0x04030201, byte);
  }

  inline void test_uint64() {
    static const uint8_t byte[] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08 };
    test<uint64_t>(0x0102030405060708ULL, 0x0807060504030201ULL, byte);
  }
}

int test_main(int, char*[]) {
  test_uint16();
  test_uint32();
  test_uint64();
  return 0;
}
