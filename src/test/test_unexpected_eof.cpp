#include "unexpected_eof.hpp"
#include <iostream>
#include "test.hpp"

namespace {
  inline void test() {
    try {
      throw vaporoid::unexpected_eof();
    } catch (const vaporoid::unexpected_eof& e) {
      std::cout
          << sizeof(vaporoid::unexpected_eof) << "\n"
          << e.what() << "\n";
      return;
    }
    BOOST_ERROR("Invalid pass");
  }
}

int test_main(int, char*[]) {
  test();
  return 0;
}
