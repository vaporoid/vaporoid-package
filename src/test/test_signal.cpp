#include "scoped_signal_action.hpp"
#include "scoped_signal_mask.hpp"
#include <unistd.h>
#include <iostream>
#include "test.hpp"

namespace {
  volatile sig_atomic_t caught_sigusr1 = 0;
  volatile sig_atomic_t caught_sigusr2 = 0;

  void handle_sigusr1(int) {
    ++caught_sigusr1;
  }

  void handle_sigusr2(int) {
    ++caught_sigusr2;
  }

  inline void test() {
    pid_t pid = getpid();
    std::cout << pid << "\n";

    sigset_t mask;
    BOOST_CHECK(sigemptyset(&mask) == 0);
    BOOST_CHECK(sigaddset(&mask, SIGUSR1) == 0);
    BOOST_CHECK(sigaddset(&mask, SIGUSR2) == 0);

    {
      vaporoid::scoped_signal_mask block(SIG_BLOCK, &mask);
      vaporoid::scoped_signal_action action_sigusr1(SIGUSR1, handle_sigusr1);
      vaporoid::scoped_signal_action action_sigusr2(SIGUSR2, handle_sigusr2);

      BOOST_CHECK(kill(pid, SIGUSR1) == 0);
      BOOST_CHECK(kill(pid, SIGUSR1) == 0);
      BOOST_CHECK(kill(pid, SIGUSR2) == 0);

      BOOST_CHECK(caught_sigusr1 == 0);
      BOOST_CHECK(caught_sigusr2 == 0);

      {
        vaporoid::scoped_signal_mask unblock(SIG_UNBLOCK, &mask);
        BOOST_CHECK(caught_sigusr1 == 1);
        BOOST_CHECK(caught_sigusr2 == 1);

        BOOST_CHECK(kill(pid, SIGUSR1) == 0);
        BOOST_CHECK(kill(pid, SIGUSR1) == 0);

        BOOST_CHECK(caught_sigusr1 == 3);
        BOOST_CHECK(caught_sigusr2 == 1);
      }
    }
  }
}

int test_main(int, char*[]) {
  test();
  return 0;
}
