#include "print.hpp"
#include <iostream>
#include <sstream>
#include "stdint.hpp"
#include "test.hpp"

namespace {
  using vaporoid::uint8_t;
  using vaporoid::uint16_t;
  using vaporoid::uint32_t;
  using vaporoid::uint64_t;

  inline void test_hex() {
    static const uint8_t data[] = { 0x01, 0x02, 0x03, 0x04 };

    std::ostringstream out;
    vaporoid::print_hex<uint8_t> (out, 42);   out << "\n";
    vaporoid::print_hex<uint16_t>(out, 42);   out << "\n";
    vaporoid::print_hex<uint32_t>(out, 42);   out << "\n";
    vaporoid::print_hex<uint64_t>(out, 42);   out << "\n";
    vaporoid::print_hex(out, data, data + 4); out << "\n";

    std::cout << out.str();
    BOOST_CHECK(out.str() == "2a\n002a\n0000002a\n000000000000002a\n01020304\n");
  }

  inline void test_lua_quote() {
    static const char data[] = "baz";

    std::ostringstream out;
    vaporoid::print_lua_quote(out, "foo\a\b\f\n\r\t\v\\\"'bar"); out << "\n";
    vaporoid::print_lua_quote(out, data, data + 4);              out << "\n";
    vaporoid::print_lua_quote(out, data);                        out << "\n";
    vaporoid::print_lua_quote(out, std::string(data, data + 4)); out << "\n";

    std::cout << out.str();
    BOOST_CHECK(out.str() == "\"foo\\a\\b\\f\\n\\r\\t\\v\\\\\\\"\\'bar\"\n\"baz\\0\"\n\"baz\"\n\"baz\\0\"\n");
  }
}

int test_main(int, char*[]) {
  test_hex();
  test_lua_quote();
  return 0;
}
