#include "system_error.hpp"
#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <algorithm>
#include <exception>
#include <iostream>
#include <string>
#include "test.hpp"

namespace {
  inline void test_no_such_file() {
    try {
      if (!fopen("", "rb")) {
        throw vaporoid::system_error("Could not fopen");
      }
    } catch (const std::exception& e) {
      std::cout
          << sizeof(vaporoid::system_error) << "\n"
          << e.what() << "\n";
      return;
    }
    BOOST_ERROR("Invalid pass");
  }

  inline void test_minus() {
    errno = -1;
    vaporoid::system_error e("");
    std::cout << e.what() << "\n";
  }

  template <size_t T_size>
  inline void test_size() {
    size_t size = 0;
    for (int i = 0; i < 256; ++i) {
      errno = i;
      vaporoid::system_error e1("");
      std::string what = e1.what();
      BOOST_CHECK(e1.code() == i);

      errno = i;
      vaporoid::system_error_detail::system_error<T_size> e2("");
      BOOST_CHECK(e2.code() == i);
      BOOST_CHECK(e2.what() == what);

      size = std::max(size, what.size() - 2);
      std::cout << i << what << "\n";
    }

    std::cout << size << "\n";
  }
}

int test_main(int, char*[]) {
  test_no_such_file();
  test_minus();
  // The GNU-specific strerror_r() function returns an "Unknown error nnn" message if the error number is unknown.
  test_size<18>();
  return 0;
}
