#! /usr/bin/env lua

local vaporoid = require("vaporoid")
local q        = vaporoid.shell.quote

local filename = arg[0] .. ".tmp"

local function generate (algorithm, size)
  do
    local h = assert(io.popen(string.format("openssl dgst -binary -%s >prepare-sha.tmp", q(algorithm)), "w"))
    for i = 1, size do
      h:write("x")
    end
    assert(h:close())
  end

  local h = assert(io.open("prepare-sha.tmp", "rb"))
  local md = h:read("*a");
  h:close()

  io.write(string.format("      { \"%s\", %4d, \"", algorithm, size));
  for i = 1, #md do
    io.write(string.format("%02x", md:byte(i)))
  end
  io.write("\" },\n")
end

io.write(string.format([[
#ifndef TEST_SHA_HPP
#define TEST_SHA_HPP

#include <stddef.h>

namespace {
  namespace test_sha {
    struct item {
      const char* algorithm;
      size_t      size;
      const char* md;
    };

    static const item data[] = {
]]))
local size = 0;
for _, a in ipairs({ "sha1"; "sha224"; "sha256"; "sha384"; "sha512" }) do
  for s = 0, 129, 3 do
    generate(a, s)
    size = size + 1
  end
  for _, s in ipairs({ 1023; 1024; 1025; 2047; 2048; 2049; 4095; 4096; 4097 }) do
    generate(a, s)
    size = size + 1
  end
end
io.write(string.format([[
    };

    static const size_t size = %d;
  }
}

#endif
]], size));

os.remove("prepare-sha.tmp");
