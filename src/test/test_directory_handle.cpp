#include "directory_handle.hpp"
#include <unistd.h>
#include <exception>
#include <iostream>
#include <string>
#include "test.hpp"

namespace {
  inline void test_no_such_directory() {
    try {
      vaporoid::directory_handle directory_handle("");
    } catch (const std::exception& e) {
      std::cout << e.what() << "\n";
      return;
    }
    BOOST_ERROR("Invalid pass");
  }

  inline void test_list() {
    std::cout
        << pathconf(".", _PC_NAME_MAX) << "\n"
        << pathconf(".", _PC_PATH_MAX) << "\n";

    vaporoid::directory_handle directory_handle(".");
    while (const char* d_name = directory_handle.read()) {
      std::cout << d_name << "\n";
    }
  }
}

int test_main(int, char*[]) {
  test_no_such_directory();
  test_list();
  return 0;
}
