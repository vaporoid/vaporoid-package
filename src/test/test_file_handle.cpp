#include "file_handle.hpp"
#include <unistd.h>
#include <string>
#include "test.hpp"

namespace {
  inline void test_stdout() {
    vaporoid::file_handle h(stdout);
    BOOST_CHECK(h.write("42", 2) == 2);
    BOOST_CHECK(h.write("\n", 1) == 1);
  }

  inline void test_writer(const std::string& filename) {
    vaporoid::file_handle h(filename.c_str(), "wb");
    BOOST_CHECK(h.write("42", 2) == 2);
  }

  inline void test_reader(const std::string& filename) {
    vaporoid::file_handle h(filename.c_str(), "rb");

    char data[2];
    BOOST_CHECK(h.tell() == 0);
    BOOST_CHECK(h.read(data, 2) == 2);
    BOOST_CHECK(data[0] == '4');
    BOOST_CHECK(data[1] == '2');
    BOOST_CHECK(h.tell() == 2);
    BOOST_CHECK(h.read(data, 2) == 0);
    BOOST_CHECK(h.tell() == 2);

    h.seek(1);
    BOOST_CHECK(h.tell() == 1);
    BOOST_CHECK(h.read(data, 2) == 1);
    BOOST_CHECK(data[0] == '2');
    BOOST_CHECK(h.tell() == 2);
    BOOST_CHECK(h.read(data, 2) == 0);
    BOOST_CHECK(h.tell() == 2);

    h.seek(-1, SEEK_CUR);
    BOOST_CHECK(h.tell() == 1);
    BOOST_CHECK(h.read(data, 2) == 1);
    BOOST_CHECK(data[0] == '2');
    BOOST_CHECK(h.tell() == 2);
    BOOST_CHECK(h.read(data, 2) == 0);
    BOOST_CHECK(h.tell() == 2);

    h.seek(0, SEEK_END);
    BOOST_CHECK(h.tell() == 2);
    BOOST_CHECK(h.read(data, 2) == 0);
    BOOST_CHECK(h.tell() == 2);
  }
}

int test_main(int, char* argv[]) {
  std::string filename = std::string(argv[0]) + ".tmp";
  test_stdout();
  test_writer(filename);
  test_reader(filename);
  unlink(filename.c_str());
  return 0;
}
