#include "execute.hpp"
#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include "system_error.hpp"
#include "test.hpp"

extern char** environ;

namespace {
  inline void test_argument_vector() {
    std::vector<std::string> args;
    args.push_back("foo");
    args.push_back("bar");
    args.push_back("baz");
    args.push_back("");
    args.push_back("42");
    vaporoid::execute_detail::argument_vector argv(args);

    char* const* a = argv.get();
    BOOST_CHECK(std::string(a[0]) == "foo");
    BOOST_CHECK(std::string(a[1]) == "bar");
    BOOST_CHECK(std::string(a[2]) == "baz");
    BOOST_CHECK(std::string(a[3]) == "");
    BOOST_CHECK(std::string(a[4]) == "42");

    BOOST_CHECK(a[0]);
    BOOST_CHECK(a[1] - a[0] == 4);
    BOOST_CHECK(a[2] - a[1] == 4);
    BOOST_CHECK(a[3] - a[2] == 4);
    BOOST_CHECK(a[4] - a[3] == 1);
    BOOST_CHECK(a[5] == 0);

    BOOST_CHECK(a[4][0] == '4');
    BOOST_CHECK(a[4][1] == '2');
    BOOST_CHECK(a[4][2] == 0);
    BOOST_CHECK(a[4][3] == 0);
  }

  inline void test_getenv() {
    std::cout << vaporoid::execute_detail::getenv(environ, "PATH") << "\n";
    BOOST_CHECK(getenv("PATH") && getenv("PATH") == vaporoid::execute_detail::getenv(environ, "PATH"));
  }

  inline void test_error(const char* program) {
    try {
      std::vector<std::string> args;
      args.push_back(program);
      vaporoid::execute(args);
    } catch (const vaporoid::system_error& e) {
      std::cout << e.what() << "\n";
      BOOST_CHECK(e.code() != 0);
      return;
    }
    BOOST_ERROR("Invalid pass");
  }
}

int test_main(int, char*[]) {
  test_argument_vector();
  test_getenv();
  test_error("/");
  test_error("");
  return 0;
}
