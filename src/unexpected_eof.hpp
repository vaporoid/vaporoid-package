#ifndef VAPOROID_UNEXPECTED_EOF_HPP
#define VAPOROID_UNEXPECTED_EOF_HPP

#include <exception>

namespace vaporoid {
  namespace unexpected_eof_detail {
    class unexpected_eof : public std::exception {
    public:
      virtual ~unexpected_eof() throw() {}

      virtual const char* what() const throw() {
        return "Unexpected EOF";
      }
    };
  }

  using unexpected_eof_detail::unexpected_eof;
}

#endif
