#ifndef VAPOROID_SCOPED_CLOSER_HPP
#define VAPOROID_SCOPED_CLOSER_HPP

#include <unistd.h>
#include <boost/noncopyable.hpp>

namespace vaporoid {
  namespace scoped_closer_detail {
    class scoped_closer : boost::noncopyable {
    public:
      explicit scoped_closer(int fd)
        : fd_(fd) {}

      ~scoped_closer() {
        close(fd_);
      }

    private:
      int fd_;
    };
  }

  using scoped_closer_detail::scoped_closer;
}

#endif
