#ifndef VAPOROID_FILE_HANDLE_HPP
#define VAPOROID_FILE_HANDLE_HPP

#include <stdio.h>
#include <algorithm>
#include <boost/noncopyable.hpp>
#include "system_error.hpp"

namespace vaporoid {
  namespace file_handle_detail {
    class file_handle : boost::noncopyable {
    public:
      explicit file_handle(FILE* handle = 0)
        : handle_(handle), close_() {}

      explicit file_handle(const char* filename, const char* mode)
        : handle_(fopen(filename, mode)), close_(&fclose) {
        if (!handle_) {
          throw system_error("Could not fopen");
        }
      }

      ~file_handle() {
        if (close_ && handle_) {
          close_(handle_);
        }
      }

      long tell() const {
        long position = ftell(handle_);
        if (position == -1) {
          throw system_error("Could not ftell");
        }
        return position;
      }

      void seek(long offset, int whence = SEEK_SET) {
        if (fseek(handle_, offset, whence) == -1) {
          throw system_error("Could not fseek");
        }
      }

      size_t read(void* data, size_t size) {
        size_t result = fread(data, 1, size, handle_);
        if (result < size && ferror(handle_)) {
          throw system_error("Could not fread");
        }
        return result;
      }

      size_t write(const void* data, size_t size) {
        size_t result = fwrite(data, 1, size, handle_);
        if (result < size && ferror(handle_)) {
          throw system_error("Could not fwrite");
        }
        return result;
      }

      void swap(file_handle& rhs) {
        std::swap(handle_, rhs.handle_);
        std::swap(close_,  rhs.close_);
      }

    private:
      FILE* handle_;
      int (*close_)(FILE*);
    };
  }

  using file_handle_detail::file_handle;
}

#endif
