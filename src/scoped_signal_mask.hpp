#ifndef VAPOROID_SCOPED_SIGNAL_MASK_HPP
#define VAPOROID_SCOPED_SIGNAL_MASK_HPP

#include <signal.h>
#include <boost/noncopyable.hpp>
#include "system_error.hpp"

namespace vaporoid {
  namespace scoped_signal_mask_detail {
    class scoped_signal_mask : boost::noncopyable {
    public:
      explicit scoped_signal_mask(int how, const sigset_t* set) {
        if (sigemptyset(&set_) == -1) {
          throw system_error("Could not sigemptyset");
        }
        if (sigprocmask(how, set, &set_) == -1) {
          throw system_error("Could not sigprocmask");
        }
      }

      ~scoped_signal_mask() {
        sigprocmask(SIG_SETMASK, &set_, 0);
      }

    private:
      sigset_t set_;
    };
  }

  using scoped_signal_mask_detail::scoped_signal_mask;
}

#endif
