#ifndef VAPOROID_DUP2_HPP
#define VAPOROID_DUP2_HPP

#include <unistd.h>
#include "system_error.hpp"

namespace vaporoid {
  inline void dup2(int source, int target) {
    if (::dup2(source, target) == -1) {
      throw system_error("Could not dup2");
    }
  }
}

#endif
