return {
  url = { "http://redis.io/" };
  require = { "GNU-make" };
  extract = "redis-2.8.8.tar.gz";
  build = [[
    vaporoid-package-make V=1 CC=gcc
    vaporoid-package-test vaporoid-package-make test
    vaporoid-package-make PREFIX="$VAPOROID_PACKAGE_PREFIX" install
  ]];
}
