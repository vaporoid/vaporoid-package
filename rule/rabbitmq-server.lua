return {
  url = {
    "http://www.rabbitmq.com/";
    "http://www.rabbitmq.com/download.html";
  };
  depend = { "otp" };
  require = { "GNU-make"; "zip"; "unzip"; "python"; "libxslt" };
  extract = "rabbitmq-server-3.3.0.tar.gz";
  patch = { "rabbitmq-server-3.3.0.vp2.patch" };
  build = [[
    vaporoid-package-make \
      TARGET_DIR="$VAPOROID_PACKAGE_PREFIX" \
      SBIN_DIR="$VAPOROID_PACKAGE_PREFIX/bin" \
      MAN_DIR="$VAPOROID_PACKAGE_PREFIX/share/man" \
      DOC_INSTALL_DIR="$VAPOROID_PACKAGE_PREFIX/share/doc/rabbitmq-server" \
        install_bin
  ]];
}
