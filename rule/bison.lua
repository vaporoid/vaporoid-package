return {
  url = { "http://ftp.gnu.org/pub/gnu/bison/" };
  depend = { "libiconv"; "GNU-m4"; "GNU-make" };
  require = { "flex"; "perl" };
  extract = "bison-3.0.2.tar.xz";
}
