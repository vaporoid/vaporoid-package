return {
  url = {
    "http://code.google.com/p/leveldb/";
    "http://code.google.com/p/leveldb/downloads/list";
    "https://bitbucket.org/vaporoid/leveldb";
  };
  depend = { "snappy" };
  require = { "GNU-make" };
  extract = "leveldb-1.15.0.tar.gz";
  patch = { "leveldb-1.15.0.vp1.patch" };
  build = [[
    CFLAGS=$CPPFLAGS
    CXXFLAGS=$CPPFLAGS
    PLATFORM_CCFLAGS=$CPPFLAGS
    PLATFORM_CXXFLAGS=$CPPFLAGS
    PLATFORM_LDFLAGS=$LDFLAGS
    export CFLAGS
    export CXXFLAGS
    export PLATFORM_CCFLAGS
    export PLATFORM_CXXFLAGS
    export PLATFORM_LDFLAGS

    system=`uname`
    case x$system in
      xDarwin)
        INSTALL_PATH=.
        export INSTALL_PATH;;
    esac

    vaporoid-package-make
    vaporoid-package-test make check

    case x$system in
      xDarwin) platform_shared_ext=dylib;;
      *)       platform_shared_ext=so;;
    esac

    cp -R include/leveldb "$VAPOROID_PACKAGE_PREFIX/include"
    cp -p "libleveldb.$platform_shared_ext.1.15" "$VAPOROID_PACKAGE_PREFIX/lib"

    cd "$VAPOROID_PACKAGE_PREFIX/lib"
    rm -f "libleveldb.$platform_shared_ext" "libleveldb.$platform_shared_ext.1"
    ln -s "libleveldb.$platform_shared_ext.1.15" "libleveldb.$platform_shared_ext"
    ln -s "libleveldb.$platform_shared_ext.1.15" "libleveldb.$platform_shared_ext.1"

    case x$system in
      xDarwin)
        cd "$VAPOROID_PACKAGE_PREFIX/lib"
        vaporoid-package-fix-install_name_tool libleveldb.$platform_shared_ext.1.15;;
    esac
  ]];
}
