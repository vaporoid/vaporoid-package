return {
  url = {
    "http://zsh.sourceforge.net/";
    "http://zsh.sourceforge.net/Arc/source.html";
    "https://bitbucket.org/vaporoid/zsh";
  };
  depend = { "gdbm"; "libiconv"; "ncurses"; "pcre" };
  require = { "GNU-make" };
  extract = "zsh-5.0.5.vp1.tar.xz";
  build = [[
    vaporoid-package-configure --enable-pcre
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
