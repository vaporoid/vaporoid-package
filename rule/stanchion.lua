return {
  url = {
    "http://docs.basho.com/riakcs/latest/";
    "http://docs.basho.com/riakcs/latest/riakcs-downloads/";
  };
  depend = { "otp" };
  require = { "GNU-make"; "bash" };
  extract = "stanchion-1.4.3.tar.gz";
  patch = { "stanchion-1.4.3.vp1.patch" };
  build = [[
    CC=gcc
    CXX=g++
    PKG_REVISION=
    PKG_VERSION=1.4.3
    export CC
    export CXX
    export PKG_REVISION
    export PKG_VERSION

    make rel

    mkdir -p \
      "$VAPOROID_PACKAGE_PREFIX/etc/stanchion" \
      "$VAPOROID_PACKAGE_PREFIX/lib/stanchion"

    root=`pwd`

    cd "$root/rel/stanchion/bin"
    cp -p * "$VAPOROID_PACKAGE_PREFIX/bin"

    cd "$root/rel/stanchion/etc"
    cp -p app.config vm.args "$VAPOROID_PACKAGE_PREFIX/etc/stanchion"

    cd "$root/rel/stanchion"
    cp -R lib erts-* releases "$VAPOROID_PACKAGE_PREFIX/lib/stanchion"
  ]];
}
