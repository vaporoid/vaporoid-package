return {
  url = {
    "http://docs.basho.com/riakcs/latest/";
    "http://docs.basho.com/riakcs/latest/riakcs-downloads/";
  };
  depend = { "otp" };
  require = { "GNU-make"; "bash" };
  extract = "riak-cs-1.4.5.tar.gz";
  patch = { "riak-cs-1.4.5.vp1.patch" };
  build = [[
    CC=gcc
    CXX=g++
    PKG_REVISION=
    PKG_VERSION=1.4.5
    export CC
    export CXX
    export PKG_REVISION
    export PKG_VERSION

    make rel

    mkdir -p \
      "$VAPOROID_PACKAGE_PREFIX/etc/riak-cs" \
      "$VAPOROID_PACKAGE_PREFIX/lib/riak-cs"

    root=`pwd`

    cd "$root/rel/riak-cs/bin"
    cp -p * "$VAPOROID_PACKAGE_PREFIX/bin"

    cd "$root/rel/riak-cs/etc"
    cp -p app.config vm.args "$VAPOROID_PACKAGE_PREFIX/etc/riak-cs"

    cd "$root/rel/riak-cs"
    cp -R lib erts-* releases "$VAPOROID_PACKAGE_PREFIX/lib/riak-cs"
  ]];
}
