return {
  url = {
    "http://ftp.gnu.org/pub/gnu/patch/";
    "http://alpha.gnu.org/gnu/patch/";
  };
  depend = { "libiconv" };
  extract = "patch-2.7.1.tar.xz";
}
