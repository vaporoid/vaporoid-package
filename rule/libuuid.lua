return {
  url = {
    "http://e2fsprogs.sourceforge.net/";
    "http://sourceforge.net/projects/e2fsprogs/files/e2fsprogs/";
    "https://bitbucket.org/vaporoid/e2fsprogs-libs";
  };
  require = { "GNU-make"; "GNU-ln" };
  extract = {
    mkdir = "libuuid";
    filename = "e2fsprogs-libs-1.42.9.vp1.tar.xz";
  };
  build = [[
    system=`uname`
    case x$system in
      xDarwin) enable_shlibs=--enable-bsd-shlibs;;
      *)       enable_shlibs=--enable-elf-shlibs;;
    esac

    vaporoid-package-configure \
      --enable-symlink-install \
      --enable-relative-symlinks \
      --enable-symlink-build \
      "$enable_shlibs"
    cd lib/uuid
    vaporoid-package-make V=1
    vaporoid-package-test vaporoid-package-make V=1 check
    vaporoid-package-make V=1 install

    case x$system in
      xDarwin)
        cd "$VAPOROID_PACKAGE_PREFIX/lib"
        vaporoid-package-fix-install_name_tool libuuid.1.1.dylib
        rm -f libuuid.dylib
        ln -s libuuid.1.1.dylib libuuid.dylib;;
    esac
  ]];
}
