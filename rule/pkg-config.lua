return {
  url = {
    "http://www.freedesktop.org/wiki/Software/pkg-config/";
    "http://pkgconfig.freedesktop.org/releases/";
  };
  depend = { "libiconv" };
  extract = "pkg-config-0.28.vp1.tar.xz";
  find = [[
    which=`vaporoid-package-which "$VAPOROID_PACKAGE_RULE" || echo ""`
    case x$which in
      x$VAPOROID_PACKAGE_PREFIX*) ;;
      *) exit 1;;
    esac
  ]];
  build = [[
    vaporoid-package-configure \
      --with-internal-glib \
      --with-libiconv=gnu
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
