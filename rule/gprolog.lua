return {
  url = {
    "http://www.gprolog.org/";
    "http://ftp.gnu.org/pub/gnu/gprolog/";
  };
  extract = "gprolog-1.4.4.tar.gz";
  build = [[
    if vaporoid-package-which-llvm gcc >/dev/null 2>&1; then
      CC=gcc-4.2
      CXX=g++-4.2
      export CC
      export CXX
    fi

    system=`uname`

    with_gas=
    case x$system in
      xOpenBSD)
        LDFLAGS="$LDFLAGS -Wl,-nopie";;
      xSunOS)
        with_gas=--with-gas;;
    esac

    cd src
    vaporoid-package-configure $with_gas
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
