return {
  url = {
    "http://www.libgd.org/";
    "https://bitbucket.org/libgd/gd-libgd/downloads";
  };
  depend = { "libiconv"; "zlib"; "libpng"; "freetype"; "fontconfig"; "libjpeg"; "tiff" };
  require = { "pkg-config"; "GNU-make" };
  extract = "libgd-2.1.0.tar.xz";
}
