return {
  url = {
    "http://www.complang.org/ragel/";
    "https://bitbucket.org/vaporoid/ragel";
  };
  extract = "ragel-6.8.tar.gz";
  build = [[
    vaporoid-package-configure
    vaporoid-package-make
    vaporoid-package-test sh -c '(cd test && vaporoid-package-make check)'
    vaporoid-package-make install
  ]];
}
