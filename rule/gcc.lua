return {
  toolchain = "gcc";
  url = {
    "http://gcc.gnu.org/";
    "https://bitbucket.org/vaporoid/gcc";
  };
  depend = { "gmp"; "mpfr"; "mpc"; "ppl"; "cloog"; "zlib"; "libiconv" };
  require = { "GNU-make"; "perl" };
  extract = "gcc-4.8.2.vp1.tar.xz";
  build = [[
    if vaporoid-package-which-llvm gcc >/dev/null 2>&1; then
      CC=gcc-4.2
      CXX=g++-4.2
      export CC
      export CXX
    fi

    system=`uname`

    enable_threads=
    enable_tls=
    with_arch=
    with_gnu_as=
    with_gnu_ld=
    case x$system in
      xOpenBSD)
        enable_threads=--enable-threads=posix
        enable_tls=--enable-tls=no
        machine=`uname -m`
        case x$machine in
          xi386)
            with_arch=--with-arch=i686;;
        esac;;
      xSunOS)
        with_gnu_as="--with-gnu-as --with-as=/usr/sfw/bin/gas"
        with_gnu_ld="--with-gnu-ld --with-ld=/usr/sfw/bin/gld";;
    esac

    mkdir -p build
    cd build
    ../configure \
      --prefix="$VAPOROID_PACKAGE_PREFIX" \
      --disable-multilib \
      $enable_threads \
      $enable_tls \
      $with_arch \
      $with_gnu_as \
      $with_gnu_ld \
      --enable-languages=c,c++ \
      --enable-cloog-backend=isl \
      --with-stage1-ldflags="$LDFLAGS" \
      --with-boot-ldflags="$LDFLAGS -static-libstdc++ -static-libgcc" \
      --with-system-zlib
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install

    case x$system in
      xDarwin) ;;
      *) vaporoid-package-fix-gcc-specs;;
    esac
  ]];
}
