return {
  url = {
    "http://www.erlang.org/";
    "http://www.erlang.org/download.html";
  };
  depend = { "openssl"; "zlib"; "ncurses" };
  require = { "GNU-make"; "perl" };
  extract = "otp-R15B01.vp2.tar.xz";
  build = [[
    if vaporoid-package-which-llvm gcc >/dev/null 2>&1; then
      CC=gcc-4.2
      CXX=g++-4.2
      export CC
      export CXX
    fi

    system=`uname`
    case x$system in
      xDarwin)
        enable_hipe=--enable-hipe
        enable_darwin_64bit=--enable-darwin-64bit;;
      xOpenBSD|xSunOS)
        DED_LD=gcc
        DED_LDFLAGS=-shared
        DED_LD_FLAG_RUNTIME_LIBRARY_PATH=-Wl,-rpath,
        export DED_LD
        export DED_LDFLAGS
        export DED_LD_FLAG_RUNTIME_LIBRARY_PATH
        enable_hipe=--disable-hipe
        enable_darwin_64bit=;;
      *)
        enable_hipe=--enable-hipe
        enable_darwin_64bit=;;
    esac

    vaporoid-package-configure \
      --enable-threads \
      --enable-smp-support \
      --enable-kernel-poll \
      $enable_hipe \
      --enable-shared-zlib \
      $enable_darwin_64bit \
      --without-javac \
      --with-ssl="$VAPOROID_PACKAGE_PREFIX" \
      --without-odbc
    vaporoid-package-make
    # [TODO]
    # vaporoid-package-test vaporoid-package-make tests
    vaporoid-package-make install
  ]];
}
