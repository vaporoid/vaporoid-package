return {
  url = {
    "http://www.sqlite.org/";
    "http://www.sqlite.org/download.html";
  };
  extract = "sqlite-doc-3080403.zip";
  build = [[
    mkdir -p "$VAPOROID_PACKAGE_PREFIX/share/doc/sqlite"
    cp -R . "$VAPOROID_PACKAGE_PREFIX/share/doc/sqlite"
  ]];
}
