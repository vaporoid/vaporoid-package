return {
  url = {
    "https://developers.google.com/protocol-buffers/";
    "http://code.google.com/p/protobuf/";
    "http://code.google.com/p/protobuf/downloads/list";
  };
  depend = { "zlib" };
  extract = "protobuf-2.5.0.tar.bz2";
  patch = { "protobuf-2.5.0.vp1.patch" };
  build = [[
    vaporoid-package-configure --disable-64bit-solaris
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
