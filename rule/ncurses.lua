return {
  url = {
    "http://ftp.gnu.org/pub/gnu/ncurses/";
    "https://bitbucket.org/vaporoid/ncurses";
  };
  extract = "ncurses-5.9.tar.gz";
  patch = { "ncurses-5.9.vp1.patch" };
  build = [[
    if vaporoid-package-which-llvm gcc >/dev/null 2>&1; then
      CC=gcc-4.2
      CXX=g++-4.2
      export CC
      export CXX
    fi

    vaporoid-package-configure --with-shared --enable-overwrite --enable-widec
    vaporoid-package-make
    vaporoid-package-make install

    cd "$VAPOROID_PACKAGE_PREFIX/lib"
    for i in libformw.* libmenuw.* libncurses++w.* libncursesw.* libpanelw.*; do
      case x$i in
        x*[0123456789]*) ;;
        *)
          head=`expr "x$i" : 'x\(.*\)w\.'`
          tail=`expr "x$i" : 'x.*w\.\(.*\)'`
          rm -f "$head.$tail"
          ln -s "$i" "$head.$tail";;
      esac
    done
    for i in libncurses.*; do
      head=libcurses
      tail=`expr "x$i" : 'xlibncurses\.\(.*\)'`
      rm -f "$head.$tail"
      ln -s "$i" "$head.$tail"
    done
  ]];
}
