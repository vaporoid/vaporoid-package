return {
  url = {
    "http://cr.yp.to/ucspi-tcp.html";
    "http://cr.yp.to/ucspi-tcp/install.html";
    "https://bitbucket.org/vaporoid/ucspi-tcp";
  };
  extract = "ucspi-tcp-0.88.tar.gz";
  patch = { "ucspi-tcp-0.88.vp1.patch" };
  build = [[
    echo "$VAPOROID_PACKAGE_PREFIX" >conf-home
    vaporoid-package-make
    vaporoid-package-make setup
    vaporoid-package-test vaporoid-package-make check
  ]];
}
