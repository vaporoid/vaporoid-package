return {
  url = {
    "http://www.sqlite.org/";
    "http://www.sqlite.org/download.html";
  };
  extract = {
    mkdir = "lemon";
    filename = "sqlite-src-3080403.zip";
  };
  build = [[
    cd tool
    gcc -O3 $CPPFLAGS lemon.c $LDFLAGS -o lemon
    cp -p lemon lempar.c "$VAPOROID_PACKAGE_PREFIX/bin"
  ]];
}
