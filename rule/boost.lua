return {
  url = {
    "http://www.boost.org/";
    "http://www.boost.org/users/download/";
    "https://bitbucket.org/vaporoid/boost";
  };
  depend = { "libiconv"; "bzip2"; "zlib"; "python" };
  extract = "boost_1_55_0.tar.gz";
  patch = {
    { level = 0; filename = "001-log_fix_dump_avx2.patch" };
    "boost-1.55.0.001.vp1.patch";
  };
  build = [[
    system=`uname`

    if vaporoid-package-compare-gcc-version gcc '<=' 4.2.1; then
      without_libraries=--without-libraries=graph_parallel,mpi,log
    else
      without_libraries=--without-libraries=graph_parallel,mpi
    fi

    define=
    case x$system in
      xOpenBSD)
        define=define=__STDC_LIMIT_MACROS
        ulimit -d "`ulimit -Hd`"
        ulimit -s "`ulimit -Hs`";;
    esac

    ./bootstrap.sh \
      $without_libraries \
      --without-icu \
      --with-python=python3 \
      --with-python-root="$VAPOROID_PACKAGE_PREFIX" \
      --prefix="$VAPOROID_PACKAGE_PREFIX"
    ./b2 -d2 \
      $define \
      dll-path="$VAPOROID_PACKAGE_PREFIX/lib" \
      boost.locale.iconv=on \
      boost.locale.icu=off \
      -sICONV_PATH="$VAPOROID_PACKAGE_PREFIX" \
      --disable-icu \
      -sBZIP2_INCLUDE="$VAPOROID_PACKAGE_PREFIX/include" \
      -sBZIP2_LIBPATH="$VAPOROID_PACKAGE_PREFIX/lib" \
      -sZLIB_INCLUDE="$VAPOROID_PACKAGE_PREFIX/include" \
      -sZLIB_LIBPATH="$VAPOROID_PACKAGE_PREFIX/lib" \
      install

    case x$system in
      xDarwin)
        cd "$VAPOROID_PACKAGE_PREFIX/lib"
        vaporoid-package-fix-install_name_tool libboost_*.dylib;;
    esac
  ]];
}
