return {
  url = {
    "https://www.python.org/";
    "https://www.python.org/downloads/source/";
  };
  depend = { "expat"; "libffi"; "gdbm"; "bzip2"; "ncurses"; "openssl"; "readline"; "sqlite"; "zlib" };
  extract = "Python-3.4.0.tar.xz";
  patch = { "python-3.4.0.vp1.patch" };
  build = [[
    vaporoid-package-configure \
      --enable-shared \
      --with-system-expat \
      --with-system-ffi \
      --without-ensurepip
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install

    case x$system in
      xDarwin) platform_shared_ext=dylib;;
      *)       platform_shared_ext=so;;
    esac

    cd "$VAPOROID_PACKAGE_PREFIX/include"
    rm -f python3.4
    ln -s python3.4m python3.4

    cd "$VAPOROID_PACKAGE_PREFIX/lib"
    rm -f "libpython3.4.$platform_shared_ext"
    ln -s "libpython3.4m.$platform_shared_ext" "libpython3.4.$platform_shared_ext"
  ]];
}
