return {
  url = {
    "http://www.libarchive.org/";
    "http://www.libarchive.org/downloads/";
    "http://libarchive.github.com/";
    "https://github.com/libarchive/libarchive";
    "https://github.com/libarchive/libarchive/tags";
    "https://bitbucket.org/vaporoid/libarchive";
  };
  depend = { "zlib"; "bzip2"; "libiconv"; "lzo"; "xz"; "openssl"; "libxml2" };
  extract = "libarchive-3.1.2.vp2.tar.xz";
  build = [[
    LIBS=-lcharset
    export LIBS

    vaporoid-package-configure
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
