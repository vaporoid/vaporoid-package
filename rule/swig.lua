return {
  url = {
    "http://www.swig.org/";
    "http://www.swig.org/download.html";
  };
  require = { "GNU-make" };
  depend = { "pcre"; "boost" };
  extract = "swig-3.0.0.tar.gz";
  build = [[
    vaporoid-package-configure
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
