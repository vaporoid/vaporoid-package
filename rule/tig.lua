return {
  url = { "http://jonas.nitro.dk/tig/releases/" };
  depend = { "git"; "ncurses"; "libiconv" };
  require = { "GNU-make"; "GNU-install" };
  extract = "tig-1.2.1.tar.gz";
  build = [[
    vaporoid-package-configure
    vaporoid-package-make
    vaporoid-package-make install
  ]];
}
