return {
  url = {
    "http://www.graphviz.org/";
    "http://www.graphviz.org/Download_source.php";
  };
  depend = { "libgd"; "zlib" };
  require = { "pkg-config"; "GNU-make"; "GNU-grep" };
  extract = "graphviz-2.36.0.tar.gz";
  patch = { "graphviz-2.36.0.vp1.patch" };
  build = [[
    LIBS=-lpthread
    export LIBS

    vaporoid-package-configure --enable-tcl=no --without-x
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
