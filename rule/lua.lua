return {
  url = {
    "http://www.lua.org/";
    "http://www.lua.org/download.html";
    "https://bitbucket.org/vaporoid/lua";
  };
  depend = { "readline" };
  extract = "lua-5.2.3.vp1.tar.xz";
  build = [[
    CC=g++
    export CC

    vaporoid-package-configure
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]]
}
