return {
  url = {
    "http://www.bzip.org/";
    "https://bitbucket.org/vaporoid/bzip2";
  };
  extract = "bzip2-1.0.6.vp1.tar.gz";
}
