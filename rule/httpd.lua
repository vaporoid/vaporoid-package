return {
  url = {
    "http://httpd.apache.org/";
    "http://httpd.apache.org/download.cgi";
  };
  depend = { "apr"; "apr-util"; "pcre"; "zlib"; "libxml2"; "lua"; "openssl" };
  extract = "httpd-2.4.9.tar.bz2";
  build = [[
    vaporoid-package-configure \
      --enable-layout=GNU \
      --enable-modules=all \
      --with-lua="$VAPOROID_PACKAGE_PREFIX" \
      --with-ssl="$VAPOROID_PACKAGE_PREFIX"
    vaporoid-package-make
    vaporoid-package-test sh -c '(cd test && vaporoid-package-make)'
    vaporoid-package-make install
  ]];
}
