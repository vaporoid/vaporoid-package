return {
  url = { "http://ftp.gnu.org/pub/gnu/autoconf/" };
  depend = { "GNU-m4"; "perl" };
  extract = "autoconf-2.69.tar.xz";
}
