return {
  url = { "http://ftp.gnu.org/pub/gnu/libsigsegv/" };
  extract = "libsigsegv-2.10.tar.gz";
  build = [[
    vaporoid-package-configure --enable-shared
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
