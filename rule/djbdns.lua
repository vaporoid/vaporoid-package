return {
  url = {
    "http://cr.yp.to/djbdns.html";
    "http://cr.yp.to/djbdns/install.html";
    "https://bitbucket.org/vaporoid/djbdns";
  };
  depend = { "ucspi-tcp" };
  extract = "djbdns-1.05.tar.gz";
  patch = { "djbdns-1.05.vp1.patch" };
  build = [[
    echo "$VAPOROID_PACKAGE_PREFIX" >conf-home
    vaporoid-package-make
    vaporoid-package-make setup
    vaporoid-package-test vaporoid-package-make check
  ]];
}
