return {
  url = { "http://libevent.org/" };
  depend = { "openssl" };
  extract = "libevent-2.1.4-alpha.tar.gz";
}
