return {
  url = {
    "http://code.google.com/p/mecab/";
    "http://code.google.com/p/mecab/downloads/list";
  };
  depend = { "libiconv" };
  extract = "mecab-0.996.tar.gz";
  build = [[
    LIBS=-liconv
    export LIBS

    vaporoid-package-configure --with-charset=utf8
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
