return {
  url = { "http://ftp.gnu.org/pub/gnu/tar/" };
  depend = { "libiconv" };
  extract = "tar-1.27.1.tar.gz";
}
