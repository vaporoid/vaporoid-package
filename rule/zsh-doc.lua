return {
  url = {
    "http://zsh.sourceforge.net/";
    "http://zsh.sourceforge.net/Arc/source.html";
    "https://bitbucket.org/vaporoid/zsh";
  };
  extract = {
    mkdir = "zsh-doc";
    chdir = "zsh-5.0.5";
    filename = "zsh-5.0.5-doc.tar.bz2";
  };
  build = [[
    cd Doc
    mkdir -p "$VAPOROID_PACKAGE_PREFIX/share/doc/zsh"
    cp -R . "$VAPOROID_PACKAGE_PREFIX/share/doc/zsh"
  ]];
}
