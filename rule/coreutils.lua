return {
  url = { "http://ftp.gnu.org/pub/gnu/coreutils/" };
  depend = { "libiconv"; "gmp" };
  extract = "coreutils-8.22.tar.xz";
}
