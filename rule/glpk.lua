return {
  url = { "http://ftp.gnu.org/pub/gnu/glpk/" };
  depend = { "gmp" };
  extract = "glpk-4.54.tar.gz";
  patch = { "glpk-4.54.vp1.patch" };
  build = [[
    vaporoid-package-configure --with-gmp
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
