return {
  build = [[
    cd "$VAPOROID_PACKAGE_ROOT/src"
    vaporoid-package-make clean
    vaporoid-package-make CXXFLAGS="-O3"
    vaporoid-package-make PREFIX="$VAPOROID_PACKAGE_PREFIX" install

    service=`lua -lvaporoid -e 'io.write(string.format("%s/service", vaporoid.pathname.dirname(vaporoid.package.prefix())))'`
    cd "$VAPOROID_PACKAGE_ROOT/src/boot"
    vaporoid-package-make clean
    vaporoid-package-make PREFIX="$VAPOROID_PACKAGE_PREFIX" SERVICE="$service"
    vaporoid-package-make PREFIX="$VAPOROID_PACKAGE_PREFIX" SERVICE="$service" install

    cd "$VAPOROID_PACKAGE_ROOT/tool"
    cp -p vaporoid-package-env vaporoid-package-resource "$VAPOROID_PACKAGE_PREFIX/bin"
  ]];
}
