return {
  url = {
    "http://cr.yp.to/daemontools.html";
    "http://cr.yp.to/daemontools/install.html";
    "https://bitbucket.org/vaporoid/daemontools";
  };
  extract = {
    mkdir = "daemontools";
    chdir = "admin/daemontools-0.76";
    filename = "daemontools-0.76.tar.gz";
  };
  patch = { "daemontools-0.76.vp1.patch" };
  build = [[
    package/compile
    cd command
    cp -p `cat ../package/commands` "$VAPOROID_PACKAGE_PREFIX/bin"
  ]];
}
