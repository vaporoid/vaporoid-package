return {
  url = {
    "http://www.gnuplot.info/";
    "http://www.gnuplot.info/download.html";
    "http://sourceforge.net/projects/gnuplot/files/gnuplot/";
  };
  depend = { "readline"; "libgd"; "lua" };
  extract = "gnuplot-4.6.5.tar.gz";
  build = [[
    vaporoid-package-configure --without-x
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
