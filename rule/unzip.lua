return {
  url = { "http://www.info-zip.org/UnZip.html" };
  extract = "unzip60.tar.gz";
  build = [[
    vaporoid-package-make -f unix/Makefile CC=gcc IZ_BZIP2=bzip2 flags
    lua -e 'io.write((io.read("*a"):gsub("-O3", "-O2")))' <flags >flags.new
    mv -f flags.new flags
    vaporoid-package-make -f unix/Makefile CC=gcc IZ_BZIP2=bzip2 generic
    vaporoid-package-test vaporoid-package-make -f unix/Makefile check
    vaporoid-package-make -f unix/Makefile prefix="$VAPOROID_PACKAGE_PREFIX" install
  ]];
}
