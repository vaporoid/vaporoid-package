return {
  url = {
    "http://www.complang.org/kelbt/";
    "https://bitbucket.org/vaporoid/kelbt";
  };
  require = { "GNU-make"; "GNU-install" };
  extract = "kelbt-0.15.tar.gz";
  patch = { "kelbt-0.15.vp1.patch" };
  build = [[
    vaporoid-package-configure
    vaporoid-package-make
    vaporoid-package-test sh -c '(cd test && vaporoid-package-make)'
    vaporoid-package-make install
  ]];
}
