return {
  url = {
    "http://apr.apache.org/";
    "http://apr.apache.org/download.cgi";
  };
  depend = { "apr"; "openssl"; "gdbm"; "sqlite"; "expat"; "libiconv" };
  extract = "apr-util-1.5.3.tar.bz2";
  build = [[
    vaporoid-package-configure \
      --with-apr="$VAPOROID_PACKAGE_PREFIX" \
      --without-apr-iconv \
      --with-crypto \
      --with-openssl \
      --without-nss \
      --without-lber \
      --without-ldap \
      --with-dbm=gdbm \
      --with-gdbm \
      --without-pgsql \
      --without-mysql \
      --without-sqlite2 \
      --without-oracle \
      --without-freetds \
      --without-odbc
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
