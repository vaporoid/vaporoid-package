return {
  url = { "http://ftp.gnu.org/pub/gnu/libidn/" };
  depend = { "libiconv" };
  extract = "libidn-1.28.tar.gz";
}
