return {
  url = { "http://ftp.gnu.org/pub/gnu/grep/" };
  depend = { "libiconv"; "pcre" };
  extract = "grep-2.18.tar.xz";
}
