return {
  url = { "http://www.ossp.org/pkg/lib/uuid/" };
  extract = "uuid-1.6.2.tar.gz";
  build = [[
    define=LIB_NAME=libossp-uuid.la

    vaporoid-package-configure
    vaporoid-package-make $define
    vaporoid-package-test vaporoid-package-make $define check
    vaporoid-package-make $define install

    cd "$VAPOROID_PACKAGE_PREFIX/lib/pkgconfig"
    mv -f uuid.pc ossp-uuid.pc

    cd "$VAPOROID_PACKAGE_PREFIX/share/man/man3"
    mv -f uuid.3 ossp-uuid.3
  ]];
}
