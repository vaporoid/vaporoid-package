return {
  url = {
    "http://mplus-fonts.sourceforge.jp/mplus-outline-fonts/";
    "http://mplus-fonts.sourceforge.jp/mplus-outline-fonts/download/";
  };
  extract = "mplus-TESTFLIGHT-058.tar.xz";
  build = [[
    mkdir -p "$VAPOROID_PACKAGE_PREFIX/share/fonts/mplus"
    cp -R . "$VAPOROID_PACKAGE_PREFIX/share/fonts/mplus"
  ]];
}
