return {
  url = {
    "http://www.nongnu.org/lzip/lzip.html";
    "http://download.savannah.gnu.org/releases/lzip/";
  };
  extract = "lzip-1.15.tar.gz";
  build = [[
    vaporoid-package-configure
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make INSTALL_PROGRAM="cp -p" INSTALL_DATA="cp -p" INSTALL_DIR="mkdir -p" install
  ]];
}
