return {
  url = {
    "http://www.openssl.org/";
    "https://bitbucket.org/vaporoid/openssl";
  };
  depend = { "zlib" };
  require = { "perl" };
  extract = "openssl-1.0.1f.tar.gz";
  patch = { "openssl-1.0.1f.vp1.patch" };
  build = [[
    system=`uname`
    case x$system in
      xDarwin)
        configure=./Configure
        os_compiler=darwin64-x86_64-cc;;
      xSunOS)
        configure=./Configure
        os_compiler=solaris-x86-gcc;;
      *)
        configure=./config
        os_compiler=;;
    esac

    "$configure" shared zlib --prefix="$VAPOROID_PACKAGE_PREFIX" $os_compiler $CPPFLAGS $LDFLAGS
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make test
    vaporoid-package-make install MANDIR=share/man
  ]];
}
