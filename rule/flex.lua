return {
  url = {
    "http://flex.sourceforge.net/";
    "http://sourceforge.net/projects/flex/files/";
  };
  depend = { "GNU-m4" };
  require = { "GNU-make" };
  extract = "flex-2.5.39.tar.xz";
  patch = { "flex-2.5.39.vp1.patch" };
}
