return {
  url = { "http://ftp.gnu.org/pub/gnu/sed/" };
  depend = { "libiconv" };
  extract = "sed-4.2.2.tar.bz2";
}
