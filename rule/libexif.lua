return {
  url = { "http://libexif.sourceforge.net/" };
  require = { "GNU-make" };
  extract = "libexif-0.6.21.tar.bz2";
}
