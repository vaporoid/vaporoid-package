return {
  url = {
    "http://code.google.com/p/mecab/";
    "http://code.google.com/p/mecab/downloads/list";
  };
  depend = { "mecab" };
  extract = "mecab-jumandic-7.0-20130310.tar.gz";
  build = [[
    vaporoid-package-configure --with-charset=utf8
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
