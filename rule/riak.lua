return {
  url = {
    "http://docs.basho.com/riak/latest/";
    "http://docs.basho.com/riak/latest/downloads/";
  };
  depend = { "otp"; "snappy" };
  require = { "GNU-make"; "GNU-patch"; "bash"; "git" };
  extract = "riak-1.4.8.vp1.tar.xz";
  build = [[
    CC=gcc
    CXX=g++
    export CC
    export CXX

    export VAPOROID_GITHUB="`pwd`/.vaporoid-github"
    lua -e 'io.write((io.read("*a"):gsub("git://github.com", os.getenv("VAPOROID_GITHUB"))))' \
      <deps/riaknostic/rebar.config \
      >deps/riaknostic/rebar.config.new
    mv -f deps/riaknostic/rebar.config.new deps/riaknostic/rebar.config

    make rel

    mkdir -p \
      "$VAPOROID_PACKAGE_PREFIX/etc/riak" \
      "$VAPOROID_PACKAGE_PREFIX/man/man1" \
      "$VAPOROID_PACKAGE_PREFIX/lib/riak"

    root=`pwd`

    cd "$root/rel/riak/bin"
    cp -p * "$VAPOROID_PACKAGE_PREFIX/bin"

    cd "$root/rel/riak/etc"
    cp -p app.config vm.args "$VAPOROID_PACKAGE_PREFIX/etc/riak"

    cd "$root/rel/riak"
    cp -R lib erts-* releases "$VAPOROID_PACKAGE_PREFIX/lib/riak"

    cd "$root/doc/man/man1"
    cp -p *.gz "$VAPOROID_PACKAGE_PREFIX/share/man/man1"
  ]];
}
