return {
  url = {
    "http://www.freedesktop.org/wiki/Software/fontconfig/";
    "http://www.freedesktop.org/software/fontconfig/release/";
  };
  depend = { "libiconv"; "freetype"; "expat"; "ipafont"; "ipaexfont"; "mplus" };
  require = { "pkg-config"; "GNU-make" };
  extract = "fontconfig-2.11.1.tar.bz2";
  build = [[
    vaporoid-package-configure \
      --with-default-fonts="$VAPOROID_PACKAGE_PREFIX/share/fonts" \
      --without-add-fonts
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
