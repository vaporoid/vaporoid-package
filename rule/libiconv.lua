return {
  url = { "http://ftp.gnu.org/pub/gnu/libiconv/" };
  extract = "libiconv-1.14.tar.gz";
  build = [[
    vaporoid-package-configure
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
    cp -p "$VAPOROID_PACKAGE_PREFIX/lib/charset.alias" "$VAPOROID_PACKAGE_PREFIX/lib/charset.alias.$VAPOROID_PACKAGE_RULE"
  ]];
}
