return {
  url = {
    "http://luajit.org/";
    "http://luajit.org/download.html";
  };
  require = { "GNU-make"; "GNU-install" };
  extract = "LuaJIT-2.0.3.tar.gz";
  build = [[
    vaporoid-package-make PREFIX="$VAPOROID_PACKAGE_PREFIX"
    vaporoid-package-make PREFIX="$VAPOROID_PACKAGE_PREFIX" install
  ]];
}
