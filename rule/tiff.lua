return {
  url = { "http://www.remotesensing.org/libtiff/" };
  depend = { "zlib"; "libjpeg"; "xz" };
  extract = "tiff-4.0.3.tar.gz";
  build = [[
    vaporoid-package-configure --without-x
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
