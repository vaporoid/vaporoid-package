return {
  url = {
    "http://bugseng.com/products/ppl/Download/";
    "http://bugseng.com/external/ppl/download/ftp/snapshots/";
  };
  depend = { "gmp"; "glpk" };
  require = { "GNU-m4"; "GNU-make" };
  extract = "ppl-1.1pre12.tar.xz";
  build = [[
    vaporoid-package-configure --enable-interfaces=c,c++
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
