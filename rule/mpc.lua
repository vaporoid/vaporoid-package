return {
  url = {
    "http://www.multiprecision.org/";
    "http://www.multiprecision.org/index.php?prog=mpc&page=download";
  };
  depend = { "gmp"; "mpfr" };
  extract = "mpc-1.0.2.tar.gz";
}
