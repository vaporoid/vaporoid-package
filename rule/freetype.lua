return {
  url = {
    "http://www.freetype.org/";
    "http://www.freetype.org/download.html";
    "http://download.savannah.gnu.org/releases/freetype/";
  };
  depend = { "zlib"; "bzip2"; "libpng" };
  require = { "pkg-config"; "GNU-make" };
  extract = "freetype-2.5.3.tar.bz2";
}
