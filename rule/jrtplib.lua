return {
  url = { "http://research.edm.uhasselt.be/~jori/page/index.php?n=CS.Jrtplib" };
  require = { "cmake" };
  extract = "jrtplib-3.9.1.tar.bz2";
  patch = { "jrtplib-3.9.1.vp1.patch" };
  build = [[
    mkdir -p build
    cd build
    env CC=gcc CXX=g++ cmake \
      -DCMAKE_BUILD_TYPE=release \
      -DCMAKE_INSTALL_PREFIX="$VAPOROID_PACKAGE_PREFIX" \
      ..
    make
    make install

    cd "$VAPOROID_PACKAGE_PREFIX/include"
    for i in jrtplib3/*.h; do
      j=`expr "x$i" : 'x.*/\(.*\)'`
      rm -f "$j"
      ln -s "$i" "$j"
    done

    system=`uname`
    case x$system in
      xDarwin)
        cd "$VAPOROID_PACKAGE_PREFIX/lib"
        vaporoid-package-fix-install_name_tool libjrtp.3.9.1.dylib;;
    esac
  ]];
}
