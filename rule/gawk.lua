return {
  url = { "http://ftp.gnu.org/pub/gnu/gawk/" };
  depend = { "readline"; "libsigsegv" };
  extract = "gawk-4.1.0.tar.xz";
}
