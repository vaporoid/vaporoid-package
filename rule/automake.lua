return {
  url = { "http://ftp.gnu.org/pub/gnu/automake/" };
  depend = { "perl"; "autoconf" };
  extract = "automake-1.14.1.tar.xz";
}
