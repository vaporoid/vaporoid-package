return {
  url = {
    "http://www.sqlite.org/";
    "http://www.sqlite.org/download.html";
  };
  depend = { "readline" };
  extract = "sqlite-autoconf-3080403.tar.gz";
}
