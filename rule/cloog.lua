return {
  url = { "ftp://gcc.gnu.org/pub/gcc/infrastructure/" };
  depend = { "gmp" };
  require = { "GNU-make"; "GNU-sed" };
  extract = "cloog-0.18.1.tar.gz";
}
