return {
  url = { "http://ipafont.ipa.go.jp/ipaexfont/download.html" };
  extract = "IPAexfont00201.zip";
  build = [[
    mkdir -p "$VAPOROID_PACKAGE_PREFIX/share/fonts/IPAexfont"
    cp -R . "$VAPOROID_PACKAGE_PREFIX/share/fonts/IPAexfont"
  ]];
}
