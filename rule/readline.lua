return {
  url = {
    "http://ftp.gnu.org/pub/gnu/readline/";
    "https://bitbucket.org/vaporoid/readline";
  };
  depend = { "ncurses" };
  extract = "readline-6.3.tar.gz";
  patch = {
    { level = 0; filename = "readline63-001" };
    { level = 0; filename = "readline63-002" };
    { level = 0; filename = "readline63-003" };
    "readline-6.3.3.vp1.patch";
  };
  build = [[
    vaporoid-package-configure --with-curses
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install

    cd "$VAPOROID_PACKAGE_PREFIX/lib"
    rm -f libreadline*.old libhistory*.old
  ]];
}
