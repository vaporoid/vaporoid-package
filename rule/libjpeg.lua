return {
  url = {
    "http://www.infai.org/jpeg/";
    "http://www.ijg.org/";
    "http://www.ijg.org/files/";
  };
  extract = {
    chdir = "jpeg-9a";
    filename = "jpegsrc.v9a.tar.gz";
  };
}
