return {
  url = { "http://zlib.net/" };
  extract = "zlib-1.2.8.tar.xz";
  build = [[
    system=`uname`
    case x$system in
      xSunOS)
        LDSHARED="gcc -shared -Wl,-h -Wl,libz.so.1"
        export LDSHARED;;
    esac

    vaporoid-package-configure
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
