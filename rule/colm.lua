return {
  url = {
    "http://www.complang.org/colm/";
    "https://bitbucket.org/vaporoid/colm";
  };
  extract = "colm-0.12.0.tar.gz";
  patch = { "colm-0.12.0.vp1.patch" };
  build = [[
    CFLAGS="-g -O2 -Wall -fPIC"
    CXXFLAGS=$CFLAGS
    export CFLAGS
    export CXXFLAGS

    vaporoid-package-configure
    vaporoid-package-make
    vaporoid-package-test sh -c '(cd test && vaporoid-package-make check)'
    vaporoid-package-make install
  ]];
}
