return {
  url = {
    "http://apr.apache.org/";
    "http://apr.apache.org/download.cgi";
  };
  depend = { "libuuid" };
  extract = "apr-1.5.0.tar.bz2";
}
