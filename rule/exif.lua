return {
  url = { "http://libexif.sourceforge.net/" };
  depend = { "libiconv"; "pkg-config"; "libexif"; "popt" };
  extract = "exif-0.6.21.tar.bz2";
}
