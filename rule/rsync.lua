return {
  url = { "http://rsync.samba.org/" };
  depend = { "libiconv"; "popt" };
  require = { "perl" };
  extract = "rsync-3.1.0.tar.gz";
}
