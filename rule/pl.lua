return {
  url = {
    "http://www.swi-prolog.org/";
    "http://www.swi-prolog.org/download/stable";
  };
  depend = { "gmp"; "readline"; "libarchive"; "openssl"; "ossp-uuid"; "zlib" };
  require = { "GNU-make" };
  extract = "pl-6.6.4.vp1.tar.xz";
  build = [[
    vaporoid-package-configure
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install

    cd packages
    vaporoid-package-configure --without-jpl --without-odbc --without-xpce
    vaporoid-package-make install PATH="$PATH"
  ]];
}
