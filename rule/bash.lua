return {
  url = { "http://ftp.gnu.org/pub/gnu/bash/" };
  depend = { "ncurses"; "readline"; "libiconv" };
  require = { "bison" };
  extract = "bash-4.3.tar.gz";
  patch = (function ()
    local patch = {}
    for i = 1, 8 do
      table.insert(patch, { level = 0; filename = string.format("bash43-%03d", i) })
    end
    return patch
  end)();
  build = [[
    vaporoid-package-configure --with-curses --with-installed-readline
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
