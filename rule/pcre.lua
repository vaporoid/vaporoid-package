return {
  url = { "http://www.pcre.org/" };
  depend = { "zlib"; "bzip2"; "readline" };
  extract = "pcre-8.34.tar.bz2";
  build = [[
    system=`uname`
    case x$system in
      xOpenBSD)
        ulimit -d "`ulimit -Hd`"
        ulimit -s "`ulimit -Hs`";;
    esac

    LIBS=-lpthread
    export LIBS

    vaporoid-package-configure \
      --enable-jit \
      --enable-utf \
      --enable-unicode-properties \
      --enable-pcregrep-libz \
      --enable-pcregrep-libbz2 \
      --enable-pcretest-libreadline
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
