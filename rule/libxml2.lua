return {
  url = { "ftp://xmlsoft.org/libxml2/"; };
  depend = { "libiconv"; "zlib"; "xz" };
  extract = "libxml2-2.9.1.tar.gz";
  build = [[
    vaporoid-package-configure --without-python
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install

    cd "$VAPOROID_PACKAGE_PREFIX/include"
    rm -f libxml
    ln -s libxml2/libxml libxml
  ]];
}
