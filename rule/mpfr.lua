return {
  url = {
    "http://www.mpfr.org/";
    "http://www.mpfr.org/mpfr-current/";
  };
  depend = { "gmp" };
  extract = "mpfr-3.1.2.tar.xz";
}
