return {
  url = {
    "http://www.cmake.org/";
    "http://www.cmake.org/cmake/resources/software.html";
  };
  depend = { "curl"; "expat"; "zlib"; "libarchive"; "expat"; "ncurses" };
  extract = "cmake-2.8.12.2.tar.gz";
  patch = { "cmake-2.8.12.2.vp1.patch" };
  build = [[
    CMAKE_PREFIX_PATH=$VAPOROID_PACKAGE_PREFIX_PATH
    export CMAKE_PREFIX_PATH

    vaporoid-package-configure --verbose --system-libs
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make test
    vaporoid-package-make install
  ]];
}
