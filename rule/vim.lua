return {
  url = {
    "http://www.vim.org/";
    "http://www.vim.org/download.php";
  };
  depend = { "ncurses"; "libiconv" };
  extract = {
    chdir = "vim74";
    filename = "vim-7.4.tar.bz2";
  };
  build = [[
    cd src
    vaporoid-package-configure \
      --disable-darwin \
      --enable-multibyte \
      --enable-gui=no \
      --disable-gpm \
      --with-features=huge \
      --without-x
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
