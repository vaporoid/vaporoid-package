return {
  url = {
    "http://curl.haxx.se/";
    "http://curl.haxx.se/download.html";
  };
  depend = { "c-ares"; "libidn"; "openssl"; "zlib"; "libiconv" };
  extract = "curl-7.36.0.tar.lzma";
  patch = { "curl-7.36.0.vp1.patch" };
  build = [[
    vaporoid-package-configure --enable-ares --disable-ldap --without-libssh2 --without-librtmp
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
