return {
  url = { "http://ftp.gnu.org/pub/gnu/m4/" };
  depend = { "libiconv"; "libsigsegv" };
  extract = "m4-1.4.17.tar.xz";
}
