return {
  url = { "http://rpm5.org/files/popt/" };
  depend = { "libiconv" };
  extract = "popt-1.16.tar.gz";
}
