return {
  url = {
    "http://www.oberhumer.com/opensource/lzo/";
    "http://www.oberhumer.com/opensource/lzo/download/";
  };
  extract = "lzo-2.06.tar.gz";
  build = [[
    vaporoid-package-configure --enable-shared
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
