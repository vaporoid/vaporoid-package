return {
  url = { "https://github.com/alanxz/rabbitmq-c" };
  depend = { "openssl"; "popt" };
  require = { "pkg-config" };
  extract = "rabbitmq-c-0.5.0.tar.gz";
  patch = { "rabbitmq-c-0.5.0.vp1.patch" };
  build = [[
    LIBS=-lpthread
    export LIBS

    vaporoid-package-configure
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
