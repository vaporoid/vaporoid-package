return {
  url = { "http://www.libpng.org/pub/png/libpng.html" };
  depend = { "zlib" };
  extract = "libpng-1.6.10.tar.xz";
}
