return {
  url = { "http://gmplib.org/" };
  depend = { "readline" };
  require = { "m4" };
  extract = {
    chdir = "gmp-6.0.0";
    filename = "gmp-6.0.0a.tar.lz";
  };
  build = [[
    system=`uname`
    case x$system in
      xSunOS)
        ABI=32
        export ABI;;
    esac

    vaporoid-package-configure --enable-cxx --with-readline
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
