return {
  url = {
    "http://www.littlecms.com/";
    "http://www.littlecms.com/download.html";
    "http://sourceforge.net/projects/lcms/files/lcms/";
  };
  depend = { "libjpeg"; "tiff" };
  extract = "lcms2-2.6.tar.gz";
}
