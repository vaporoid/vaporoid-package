return {
  url = {
    "http://www.perl.org/";
    "http://www.perl.org/get.html";
  };
  extract = "perl-5.18.2.tar.gz";
  build = [[
    ./Configure -de \
      -Dprefix="$VAPOROID_PACKAGE_PREFIX" \
      -Duseshrplib \
      -Dcc=gcc \
      -Accflags=-fPIC \
      -Dlibs=-lm
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make test
    vaporoid-package-make install
  ]];
}
