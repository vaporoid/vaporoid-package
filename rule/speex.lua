return {
  url = {
    "http://www.speex.org/";
    "http://www.speex.org/downloads/";
  };
  depend = { "libogg" };
  extract = "speex-1.2rc1.tar.gz";
}
