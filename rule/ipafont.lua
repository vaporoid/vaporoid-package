return {
  url = { "http://ipafont.ipa.go.jp/ipafont/download.html" };
  extract = "IPAfont00303.zip";
  build = [[
    mkdir -p "$VAPOROID_PACKAGE_PREFIX/share/fonts/IPAfont"
    cp -R . "$VAPOROID_PACKAGE_PREFIX/share/fonts/IPAfont"
  ]];
}
