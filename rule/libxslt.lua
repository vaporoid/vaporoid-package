return {
  url = {
    "ftp://xmlsoft.org/libxml2/";
    "https://bitbucket.org/vaporoid/libxslt";
  };
  depend = { "libgcrypt"; "libxml2" };
  extract = "libxslt-1.1.28.tar.gz";
  patch = { "libxslt-1.1.28.vp1.patch" };
  build = [[
    vaporoid-package-configure --without-python
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
