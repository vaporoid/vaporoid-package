return {
  url = {
    "http://www.gnupg.org/download/";
    "http://www.gnupg.org/download/#sec-1-6";
  };
  depend = { "libgpg-error" };
  extract = "libgcrypt-1.6.1.tar.bz2";
  build = [[
    if vaporoid-package-which-llvm gcc >/dev/null 2>&1; then
      CC=gcc-4.2
      CXX=g++-4.2
      export CC
      export CXX
    fi

    system=`uname`
    case x$system in
      xOpenBSD)
        disable_amd64_as_feature_detection=--disable-amd64-as-feature-detection;;
      *)
        disable_amd64_as_feature_detection=;;
    esac

    vaporoid-package-configure $disable_amd64_as_feature_detection
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make check
    vaporoid-package-make install
  ]];
}
