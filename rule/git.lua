return {
  url = {
    "http://git-scm.com/";
    "http://code.google.com/p/git-core/downloads/list";
  };
  depend = { "openssl"; "pcre"; "curl"; "expat"; "libiconv"; "perl"; "zlib"; "python" };
  require = { "GNU-make"; "GNU-install"; "autoconf" };
  extract = "git-1.9.1.tar.gz";
  build = [[
    CC=gcc
    export CC

    echo INSTALL=install                   >config.mak
    echo NO_APPLE_COMMON_CRYPTO=YesPlease >>config.mak
    echo NO_GETTEXT=YesPlease             >>config.mak

    vaporoid-package-make configure
    vaporoid-package-configure \
      --with-libpcre \
      --with-perl="$VAPOROID_PACKAGE_PREFIX/bin/perl" \
      --with-python="$VAPOROID_PACKAGE_PREFIX/bin/python3" \
      --without-tcltk
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make test
    vaporoid-package-make install
  ]];
}
