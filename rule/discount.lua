return {
  url = { "http://www.pell.portland.or.us/~orc/Code/discount/" };
  extract = "discount-2.1.6.tar.bz2";
  build = [[
    CC=gcc
    export CC

    ./configure.sh --prefix="$VAPOROID_PACKAGE_PREFIX"
    vaporoid-package-make
    vaporoid-package-test vaporoid-package-make test
    cp -p markdown "$VAPOROID_PACKAGE_PREFIX/bin"
  ]];
}
