return {
  url = {
    "http://docs.basho.com/riakcs/latest/";
    "http://docs.basho.com/riakcs/latest/riakcs-downloads/";
  };
  depend = { "otp" };
  require = { "GNU-make"; "bash" };
  extract = "riak-cs-control-1.0.2.tar.gz";
  patch = { "riak-cs-control-1.0.2.vp1.patch" };
  build = [[
    CC=gcc
    CXX=g++
    PKG_REVISION=
    PKG_VERSION=1.0.2
    export CC
    export CXX
    export PKG_REVISION
    export PKG_VERSION

    make rel

    mkdir -p \
      "$VAPOROID_PACKAGE_PREFIX/etc/riak-cs-control" \
      "$VAPOROID_PACKAGE_PREFIX/lib/riak-cs-control"

    root=`pwd`

    cd "$root/rel/riak-cs-control/bin"
    cp -p * "$VAPOROID_PACKAGE_PREFIX/bin"

    cd "$root/rel/riak-cs-control/etc"
    cp -p app.config vm.args "$VAPOROID_PACKAGE_PREFIX/etc/riak-cs-control"

    cd "$root/rel/riak-cs-control"
    cp -R lib erts-* releases "$VAPOROID_PACKAGE_PREFIX/lib/riak-cs-control"
  ]];
}
